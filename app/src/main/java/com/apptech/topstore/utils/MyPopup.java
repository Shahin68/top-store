package com.apptech.topstore.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.PopupMenu;

import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.CalendarType;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.databaseLocal.Currency;
import com.apptech.topstore.databaseLocal.Languages;
import com.apptech.topstore.databaseLocal.State;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;
import java.util.List;

public class MyPopup {
    public static PowerMenu countryCodeMenu(Context context, View view){
        int count = Country.countryNames().length;
        List<PowerMenuItem> menuItems = new ArrayList<>();
        for (int i = 0; i < count; i++){
            PowerMenuItem menuItem = new PowerMenuItem(
                    ConvertNumbers.parseNumbers(Country.countryNames()[i].second, false),
                    Country.countryFlags()[i],
                    i
            );
            menuItems.add(menuItem);
        }
        PowerMenu powerMenu = new PowerMenu.Builder(context)
                .addItemList(menuItems)
                .setTextColor(ContextCompat.getColor(context, R.color.textDark))
                .setTextTypeface(Fonts.BOLD(context))
                .setMenuColor(ContextCompat.getColor(context, R.color.white))
                .setMenuRadius(26f)
                .setMenuShadow(16f)
                .setTextSize(16)
                .setTextGravity(Gravity.END)
                .setShowBackground(false)
                .setSelectedEffect(false)
                .setAutoDismiss(true)
                .setWidth(800)
                .build();
        if (count <= 5){
            powerMenu.setAnimation(MenuAnimation.DROP_DOWN);
            powerMenu.showAsDropDown(view);
        } else {
            powerMenu.setAnimation(MenuAnimation.FADE);
            powerMenu.showAtCenter(view);
        }
        return powerMenu;
    }
    public static PowerMenu countryNameMenu(Context context, View view){
        int count = Country.countryNames().length;
        List<PowerMenuItem> menuItems = new ArrayList<>();
        for (int i = 0; i < count; i++){
            PowerMenuItem menuItem = new PowerMenuItem(
                    ConvertNumbers.parseNumbers(Country.countryNames()[i].first, false),
                    Country.countryFlags()[i],
                    i
            );
            menuItems.add(menuItem);
        }
        PowerMenu powerMenu = new PowerMenu.Builder(context)
                .addItemList(menuItems)
                .setTextColor(ContextCompat.getColor(context, R.color.textDark))
                .setTextTypeface(Fonts.BOLD(context))
                .setMenuColor(ContextCompat.getColor(context, R.color.white))
                .setMenuRadius(26f)
                .setMenuShadow(16f)
                .setTextSize(16)
                .setTextGravity(Gravity.END)
                .setShowBackground(false)
                .setSelectedEffect(false)
                .setAutoDismiss(true)
                .setWidth(800)
                .build();
        if (count <= 5){
            powerMenu.setAnimation(MenuAnimation.DROP_DOWN);
            powerMenu.showAsDropDown(view);
        } else {
            powerMenu.setAnimation(MenuAnimation.FADE);
            powerMenu.showAtCenter(view);
        }
        return powerMenu;
    }
    public static PopupMenu currencyMenu(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Currency.countryCurrencies(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Currency.countryCurrencies(activity)[i].first);
        }
        popup.show();
        return popup;
    }
    public static PopupMenu calendarMenu(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = CalendarType.get(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    CalendarType.get(activity)[i]);
        }
        popup.show();
        return popup;
    }
    public static PopupMenu languageMenu(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Languages.get(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Languages.get(activity)[i]);
        }
        popup.show();
        return popup;
    }
    public static PopupMenu stateMenu(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = State.get(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    State.get(activity)[i]);
        }
        popup.show();
        return popup;
    }
}
