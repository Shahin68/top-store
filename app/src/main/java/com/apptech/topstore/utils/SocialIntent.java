package com.apptech.topstore.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import android.widget.Toast;

import com.apptech.topstore.R;


public class SocialIntent {
    public static void openTelegram(Context context, String url){
        if (!url.equals("")){
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo("org.telegram.messenger",0);
                if (info.applicationInfo.enabled){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent.resolveActivity(context.getPackageManager()) != null){
                        context.startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    if (intent.resolveActivity(context.getPackageManager()) != null){
                        context.startActivity(intent);
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }
                e.printStackTrace();
            }
        } else {
            MyToast.warning(context.getString(R.string.link_unavailable)).show();
        }
    }
    public static void openInstagram(Context context, String url){
        if (!url.equals("")){
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo("com.instagram.android",0);
                if (info.applicationInfo.enabled){
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent.resolveActivity(context.getPackageManager()) != null){
                        context.startActivity(intent);
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }
                e.printStackTrace();
            }
        } else {
            MyToast.warning(context.getString(R.string.link_unavailable)).show();
        }
    }
    public static void openFacebook(Context context, String url){
        if (!url.equals("")){
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo("com.facebook.katana",0);
                if (info.applicationInfo.enabled){
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent.resolveActivity(context.getPackageManager()) != null){
                        context.startActivity(intent);
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }
                e.printStackTrace();
            }
        } else {
            MyToast.warning(context.getString(R.string.link_unavailable)).show();
        }
    }
    public static void openTwitter(Context context, String url){
        if (!url.equals("")){
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo("com.twitter.android",0);
                if (info.applicationInfo.enabled){
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                    context.startActivity(intent);
                }
            } catch (PackageManager.NameNotFoundException e) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }
                e.printStackTrace();
            }
        } else {
            MyToast.warning(context.getString(R.string.link_unavailable)).show();
        }
    }
    public static void call(Context activity, String number){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + number));
        activity.startActivity(callIntent);
    }
}
