/*
 * Created by shahin montazeri (shahin.montazeri@gmail.com)
 * Copyright (c) 2018 at mediahamrah. All rights reserved.
 */

package com.apptech.topstore.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Locale;

public class Fonts {

    // ---------------- Iran Sans Fonts ------------------------------------------------------------

    public static Typeface NORMAL(Context context){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans.ttf");
        } else {
            return Typeface.createFromAsset(context.getAssets(), "fonts/latin_regular.ttf");
        }
    }

    public static Typeface LIGHT(Context context){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans_light.ttf");
        } else {
            return Typeface.createFromAsset(context.getAssets(), "fonts/latin_light.ttf");
        }
    }

    public static Typeface BOLD(Context context){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans_bold.ttf");
        } else {
            return Typeface.createFromAsset(context.getAssets(), "fonts/latin_bold.ttf");
        }
    }

}
