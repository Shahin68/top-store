package com.apptech.topstore.utils;

import android.icu.util.Calendar;

import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.common.CalendarFactory;
import com.aminography.primecalendar.common.CalendarType;

import java.sql.Time;
import java.util.Date;
import java.util.Locale;

public class Today {
    public String weekDay(){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            primeCalendar.setTimeInMillis(System.currentTimeMillis());
            return primeCalendar.getWeekDayName();
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            primeCalendar.setTimeInMillis(System.currentTimeMillis());
            return primeCalendar.getWeekDayName();
        }
    }
    public String date(){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            primeCalendar.setTimeInMillis(System.currentTimeMillis());
            return primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear();
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            primeCalendar.setTimeInMillis(System.currentTimeMillis());
            return primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear();
        }
    }
}
