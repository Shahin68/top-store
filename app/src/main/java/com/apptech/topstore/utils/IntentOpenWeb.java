package com.apptech.topstore.utils;

import android.content.Intent;
import android.net.Uri;

import com.apptech.topstore.AppController;


public class IntentOpenWeb {
    public static void runIntent(String target){
        Uri webpage = Uri.parse(target);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (intent.resolveActivity(AppController.getContext().getPackageManager()) != null) {
            AppController.getContext().startActivity(intent);
        }
    }
}
