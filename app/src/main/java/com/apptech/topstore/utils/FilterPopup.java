package com.apptech.topstore.utils;

import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;

import com.apptech.topstore.R;

public class FilterPopup {
    public static PopupMenu getProductFilters(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = forProducts(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    forProducts(activity)[i]);
        }
        popup.show();
        return popup;
    }
    public static String[] forProducts(Context context){
        String[] strings = new String[]{
                context.getString(R.string.most_available),
                context.getString(R.string.least_available),
                context.getString(R.string.most_expensive),
                context.getString(R.string.least_expensive),
                context.getString(R.string.best_selling),
                context.getString(R.string.remove_filter)
        };
        return strings;
    }

    public static PopupMenu getFactorFilters(Context activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = forFactors(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    forFactors(activity)[i]);
        }
        popup.show();
        return popup;
    }
    public static String[] forFactors(Context context){
        String[] strings = new String[]{
                context.getString(R.string.newest),
                context.getString(R.string.oldest),
                context.getString(R.string.most_expensive),
                context.getString(R.string.least_expensive),
                context.getString(R.string.remove_filter)
        };
        return strings;
    }
}
