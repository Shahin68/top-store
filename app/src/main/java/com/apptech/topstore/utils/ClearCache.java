package com.apptech.topstore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import java.io.File;

// -- This Class Will CLear All App Cache and Shared Preferences
public class ClearCache {
    public static void clearNow(Context context){
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
            MyLog.LogInformation("CACHED CLEARED");
        } catch (Exception f){
            MyLog.LogInformation("CACHED NOT CLEARED");
            f.printStackTrace();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
