package com.apptech.topstore.utils;

import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.common.CalendarFactory;
import com.aminography.primecalendar.common.CalendarType;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateConverter {
    public static String weekDay(long time){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            primeCalendar.setTimeInMillis(time);
            return primeCalendar.getWeekDayName();
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            primeCalendar.setTimeInMillis(time);
            return primeCalendar.getWeekDayName();
        }
    }
    public static String date(long time){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            primeCalendar.setTimeInMillis(time);
            return primeCalendar.getWeekDayName() + " - " +primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear() + " - " + primeCalendar.get(Calendar.HOUR_OF_DAY) + ":" + primeCalendar.get(Calendar.MINUTE);
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            primeCalendar.setTimeInMillis(time);
            return primeCalendar.getWeekDayName() + " - " +primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear() + " - " + primeCalendar.get(Calendar.HOUR_OF_DAY) + ":" + primeCalendar.get(Calendar.MINUTE);
        }
    }



    public static PrimeCalendar[] getLast7Days(){
        Date date = new Date();
        PrimeCalendar[] calendars;

        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar calendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);


            PrimeCalendar calendar0 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar0.setTime(date);

            PrimeCalendar calendar1 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar1.setTime(date);
            calendar1.add(Calendar.DATE, -1);

            PrimeCalendar calendar2 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar2.setTime(date);
            calendar2.add(Calendar.DATE, -2);

            PrimeCalendar calendar3 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar3.setTime(date);
            calendar3.add(Calendar.DATE, -3);

            PrimeCalendar calendar4 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar4.setTime(date);
            calendar4.add(Calendar.DATE, -4);

            PrimeCalendar calendar5 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar5.setTime(date);
            calendar5.add(Calendar.DATE, -5);

            PrimeCalendar calendar6 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar6.setTime(date);
            calendar6.add(Calendar.DATE, -6);


            calendars = new PrimeCalendar[]{
                    calendar,
                    calendar0,
                    calendar1,
                    calendar2,
                    calendar3,
                    calendar4,
                    calendar5,
                    calendar6,
            };
        } else {
            PrimeCalendar calendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 1);


            PrimeCalendar calendar0 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar0.setTime(date);

            PrimeCalendar calendar1 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar1.setTime(date);
            calendar1.add(Calendar.DATE, -1);

            PrimeCalendar calendar2 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar2.setTime(date);
            calendar2.add(Calendar.DATE, -2);

            PrimeCalendar calendar3 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar3.setTime(date);
            calendar3.add(Calendar.DATE, -3);

            PrimeCalendar calendar4 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar4.setTime(date);
            calendar4.add(Calendar.DATE, -4);

            PrimeCalendar calendar5 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar5.setTime(date);
            calendar5.add(Calendar.DATE, -5);

            PrimeCalendar calendar6 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar6.setTime(date);
            calendar6.add(Calendar.DATE, -6);


            calendars = new PrimeCalendar[]{
                    calendar,
                    calendar0,
                    calendar1,
                    calendar2,
                    calendar3,
                    calendar4,
                    calendar5,
                    calendar6,
            };
        }




        return calendars;
    }
    public static String forDayComparison(long time){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            Date date = new Date(Float.valueOf(time).longValue());
            primeCalendar.setTime(date);
            return primeCalendar.getWeekDayName();
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            Date date = new Date(Float.valueOf(time).longValue());
            primeCalendar.setTime(date);
            return primeCalendar.getWeekDayName();
        }
    }
    public static PrimeCalendar[] getLast12Months(){
        Date date = new Date();
        PrimeCalendar[] calendars;

        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar calendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar.setTime(date);

            PrimeCalendar calendar1 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar1.setTime(date);
            calendar1.add(Calendar.MONTH, -1);

            PrimeCalendar calendar2 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar2.setTime(date);
            calendar2.add(Calendar.MONTH, -2);

            PrimeCalendar calendar3 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar3.setTime(date);
            calendar3.add(Calendar.MONTH, -3);

            PrimeCalendar calendar4 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar4.setTime(date);
            calendar4.add(Calendar.MONTH, -4);

            PrimeCalendar calendar5 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar5.setTime(date);
            calendar5.add(Calendar.MONTH, -5);

            PrimeCalendar calendar6 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar6.setTime(date);
            calendar6.add(Calendar.MONTH, -6);

            PrimeCalendar calendar7 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar7.setTime(date);
            calendar7.add(Calendar.MONTH, -7);

            PrimeCalendar calendar8 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar8.setTime(date);
            calendar8.add(Calendar.MONTH, -8);

            PrimeCalendar calendar9 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar9.setTime(date);
            calendar9.add(Calendar.MONTH, -9);

            PrimeCalendar calendar10 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar10.setTime(date);
            calendar10.add(Calendar.MONTH, -10);

            PrimeCalendar calendar11 = CalendarFactory.newInstance(CalendarType.PERSIAN);
            calendar11.setTime(date);
            calendar11.add(Calendar.MONTH, -11);


            calendars = new PrimeCalendar[]{
                    calendar,
                    calendar1,
                    calendar2,
                    calendar3,
                    calendar4,
                    calendar5,
//                calendar6,
//                calendar7,
//                calendar8,
//                calendar9,
//                calendar10,
//                calendar11
            };
        } else {
            PrimeCalendar calendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar.setTime(date);

            PrimeCalendar calendar1 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar1.setTime(date);
            calendar1.add(Calendar.MONTH, -1);

            PrimeCalendar calendar2 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar2.setTime(date);
            calendar2.add(Calendar.MONTH, -2);

            PrimeCalendar calendar3 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar3.setTime(date);
            calendar3.add(Calendar.MONTH, -3);

            PrimeCalendar calendar4 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar4.setTime(date);
            calendar4.add(Calendar.MONTH, -4);

            PrimeCalendar calendar5 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar5.setTime(date);
            calendar5.add(Calendar.MONTH, -5);

            PrimeCalendar calendar6 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar6.setTime(date);
            calendar6.add(Calendar.MONTH, -6);

            PrimeCalendar calendar7 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar7.setTime(date);
            calendar7.add(Calendar.MONTH, -7);

            PrimeCalendar calendar8 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar8.setTime(date);
            calendar8.add(Calendar.MONTH, -8);

            PrimeCalendar calendar9 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar9.setTime(date);
            calendar9.add(Calendar.MONTH, -9);

            PrimeCalendar calendar10 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar10.setTime(date);
            calendar10.add(Calendar.MONTH, -10);

            PrimeCalendar calendar11 = CalendarFactory.newInstance(CalendarType.CIVIL);
            calendar11.setTime(date);
            calendar11.add(Calendar.MONTH, -11);


            calendars = new PrimeCalendar[]{
                    calendar,
                    calendar1,
                    calendar2,
                    calendar3,
                    calendar4,
                    calendar5,
//                calendar6,
//                calendar7,
//                calendar8,
//                calendar9,
//                calendar10,
//                calendar11
            };
        }



        return calendars;
    }
    public static String forMonthComparison(long time){
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
            Date date = new Date(Float.valueOf(time).longValue());
            primeCalendar.setTime(date);
            return primeCalendar.getMonthName();
        } else {
            PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.CIVIL);
            Date date = new Date(Float.valueOf(time).longValue());
            primeCalendar.setTime(date);
            return primeCalendar.getMonthName();
        }
    }
}
