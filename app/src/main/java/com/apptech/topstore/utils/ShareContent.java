package com.apptech.topstore.utils;


import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import com.apptech.topstore.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ShareContent {

    //region Share Text With Single Image
    public static void shareWithUri(Context context, String newsTitle, String newsContent, String imageUri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imageUri));
        shareIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\n" + newsContent);
        shareIntent.setType("image/*");
        // shareIntent.setData(Uri.parse(uriToImage));
        if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(
                    Intent.createChooser(shareIntent, context.getText(R.string.share_with)));
        }
    }
    //endregion

    //region Share Multiple Images with Text
    public static void shareMultiple(Context context, String newsTitle, String newsContent, List<String> uris) {
        MyLog.LogWeird("SHARE STARTED");
        shareWithUriMultiple(context, newsTitle, newsContent, uris);
    }
    private static void shareWithUriMultiple(Context context, String newsTitle, String newsContent, List<String> uris) {
        if (uris.size() != 1) {
            //region Description
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            shareIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\n" + newsContent);

            shareIntent.setType("image/*");

            ClipData clipData = null;
            ArrayList<Uri> files = new ArrayList<>();
            for (int i =0; i<uris.size();i++) {
                File file = new File(uris.get(i));
                Uri uri = Uri.fromFile(file);
                if (i==0){
                    clipData = ClipData.newUri(context.getContentResolver(), newsTitle + "\n" + newsContent, uri);
                }else {
                    clipData.addItem(new ClipData.Item(uri));
                }
                files.add(uri);
            }
            shareIntent.setClipData(clipData);
            shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);

            if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(
                        Intent.createChooser(shareIntent, context.getString(R.string.share_with)));
            }
            //endregion
        }else {

            File file = new File(uris.get(0));
            Uri contentUri = Uri.fromFile(file);

            if (contentUri != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.addFlags(
                        Intent
                                .FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app
                // to read this file
                shareIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\n" + newsContent);
                shareIntent.setClipData(ClipData.newRawUri("EVERPICS", contentUri));
//            shareIntent.setDataAndType(
//                    contentUri, context.getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(Intent.createChooser(shareIntent,  context.getString(R.string.share_with)));
                }
            }
        }
    }
    //endregion


}
