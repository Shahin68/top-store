/*
 * Programmed by MobileMedia Communication Company
 * Copyright (c) 2018 at MobileMedia Communication. All rights reserved.
 */

package com.apptech.topstore.utils;

import android.util.Log;

import com.apptech.topstore.BuildConfig;

public class MyLog {
    private static final String LOG_TAG = "TopStoreLog";

    public static void LogError(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.e(LOG_TAG, result);
        }
    }

    public static void LogWeird(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.w(LOG_TAG, result);
        }
    }

    public static void LogInformation(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.i(LOG_TAG, result);
        }
    }

    public static void LogDebugging(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.d(LOG_TAG, result);
        }
    }

    public static void LogEverything(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.v(LOG_TAG, result);
        }
    }

    public static void LogDisaster(String result){
        if (BuildConfig.ACTIVATE_LOGS){
            Log.wtf(LOG_TAG, result);
        }
    }

}
