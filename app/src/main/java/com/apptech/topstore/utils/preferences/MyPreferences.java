package com.apptech.topstore.utils.preferences;

public class MyPreferences {
    public static final String USER_ID = "user_id"; // string value - user phone number
    public static final String USER_PHONE = "user_phone"; // string value - user phone number
    public static final String USER_NAME = "user_name"; // string value - user name
    public static final String USER_STORE_NAME = "user_store_name"; // string value - user name
    public static final String USER_SALES_COUNT = "sales_count"; // integer value - number of sales
    public static final String USER_PURCHASE_COUNT = "purchase_count"; // integer value - number of purchases

    public static final String USER_CURRENCY = "user_currency"; //
    public static final String USER_COUNTRY = "user_country"; //
    public static final String USER_CALENDAR_TYPE = "user_calendar_type"; //
    public static final String USER_ACTIVE_NOTIFICATION = "user_active_notification"; // string value - user notification activation state
    public static final String USER_LANGUAGE = "user_language"; //
    public static final String USER_LATITUDE = "user_latitude"; //
    public static final String USER_LONGITUDE = "user_longitude"; //
    public static final String USER_COUNTRY_CODE = "user_country_code"; // integer value - user country code number
    public static final String USER_COUNTRY_FLAG = "user_country_flag"; // integer value - user country Flag number


    public static final String USER_TOKEN = "user_token"; // string value - user oken
}
