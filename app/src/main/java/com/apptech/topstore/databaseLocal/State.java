package com.apptech.topstore.databaseLocal;

import android.content.Context;

import com.apptech.topstore.R;

public class State {
    public static String[] get(Context context){
        String[] strings = new String[]{
                context.getString(R.string.activated),
                context.getString(R.string.deactivated)
        };
        return strings;
    }
}
