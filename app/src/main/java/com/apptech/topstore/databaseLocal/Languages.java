package com.apptech.topstore.databaseLocal;

import android.content.Context;
import android.content.res.Resources;

import com.apptech.topstore.R;

public class Languages {
    public static String[] get(Context context){
        String[] strings = new String[]{
                context.getString(R.string.farsi),
                context.getString(R.string.english)
        };
        return strings;
    }
}
