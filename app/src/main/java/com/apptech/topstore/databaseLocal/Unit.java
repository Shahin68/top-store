package com.apptech.topstore.databaseLocal;

import android.content.Context;
import android.util.Pair;

import com.apptech.topstore.R;

public class Unit {
    public static Pair<String, Integer>[] units(Context context){
        Pair<String, Integer>[] pairs = new Pair[]{
                Pair.create(context.getString(R.string.unit_number), 0),
                Pair.create(context.getString(R.string.unit_kilo), 1),
                Pair.create(context.getString(R.string.unit_gram), 2),
                Pair.create(context.getString(R.string.unit_pack), 3),
                Pair.create(context.getString(R.string.unit_bottle), 4),
                Pair.create(context.getString(R.string.unit_meschal), 5),
                Pair.create(context.getString(R.string.unit_sack), 6)
        };
        return pairs;
    }
    public static String getUnit(Context context, int index){
        String[] strings = new String[]{
                context.getString(R.string.unit_number),
                context.getString(R.string.unit_kilo),
                context.getString(R.string.unit_gram),
                context.getString(R.string.unit_pack),
                context.getString(R.string.unit_bottle),
                context.getString(R.string.unit_meschal),
                context.getString(R.string.unit_sack)
        };
        return strings[index];
    }
}
