package com.apptech.topstore.databaseLocal;


import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.utils.MyLog;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.InputStream;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;


public class InsertProducts extends AsyncTask<Context, Void, Void>{

    @Inject
    AppDatabase appDatabase;


    @Override
    protected Void doInBackground(Context... contexts) {

        Sheet sheet = null;
        try {
            AssetManager assetManager = contexts[0].getAssets();
            InputStream inputStream = assetManager.open("products.xls");
            Workbook workbook = Workbook.getWorkbook(inputStream);
            sheet = workbook.getSheet(0);
        } catch (Exception f){
            f.printStackTrace();
            return null;
        }
        // getCell(col, row)
        int rowCount = sheet.getRows();
        for (int row = 1; row < rowCount; row++){ // 50000
            try {
                insert(new ProductModel(
                        String.valueOf(row),
                        sheet.getCell(5, row).getContents(),
                        sheet.getCell(1, row).getContents(),
                        "",
                        Integer.parseInt(sheet.getCell(3, row).getContents()),
                        Integer.parseInt(sheet.getCell(4, row).getContents()),
                        sheet.getCell(2, row).getContents(),
                        1,
                        1,
                        false
                ), row);
            } catch (Exception f){
                f.printStackTrace();
                MyLog.LogDebugging("INSERT EXCEL -------> " + "Exception " + f);
            }
        }
        MyLog.LogDebugging("INSERT EXCEL -------> " + "DONE()");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

    }

    private void insert(ProductModel productModel, int index) {
         insertObservable(productModel)
                .onBackpressureLatest()
                .observeOn(Schedulers.computation())
                .subscribe(new FlowableSubscriber<Void>() {
                    @Override
                    public void onSubscribe(Subscription s) {

                        MyLog.LogDebugging("INSERT EXCEL -------> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable t) {
                        MyLog.LogDebugging("INSERT EXCEL -------> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        MyLog.LogDebugging("INSERT EXCEL -------> " + "onComplete()  -  " + index);
                    }
                });
    }
    private Flowable<Void> insertObservable(ProductModel productModel) {
        return new Flowable<Void>() {
            @Override
            protected void subscribeActual(Subscriber<? super Void> observer) {
                appDatabase.productsDao().insertOne(productModel);
                observer.onSubscribe(new Subscription() {
                    @Override
                    public void request(long n) {

                    }

                    @Override
                    public void cancel() {

                    }
                });
                observer.onComplete();
            }
        };
    }

}
