package com.apptech.topstore.databaseLocal;

import android.content.Context;

import com.apptech.topstore.R;

public class CalendarType {
    public static String[] get(Context context){
        String[] strings = new String[]{
                context.getString(R.string.shamsi),
                context.getString(R.string.Gregorian)
        };
        return strings;
    }
}
