package com.apptech.topstore;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.apptech.topstore.components.AppComponents;
import com.apptech.topstore.components.DaggerAppComponents;
import com.apptech.topstore.databaseLocal.InsertProducts;
import com.apptech.topstore.modules.AppModule;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppController extends Application {
    private AppComponents mAppComponent;
    public AppComponents getAppComponent() {
        return mAppComponent;
    }

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        AppController.context = context;
    }

    public static AppController get(Context context) {
        return (AppController) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        setContext(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mAppComponent = DaggerAppComponents.builder().appModule(new AppModule(this)).build();


        MySharedPreferences.setPreferences(this);
        if (Locale.getDefault().getLanguage().startsWith("fa") || Locale.getDefault().getLanguage().startsWith("ar")){
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/iran_sans_farsi_number.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/latin_regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }


        // ------ make this better
        if (MySharedPreferences.getString(MyPreferences.USER_TOKEN, "").isEmpty()){
            InsertProducts products = new InsertProducts();
            mAppComponent.MyInject(products);
            products.execute(getApplicationContext());
        }
    }
}
