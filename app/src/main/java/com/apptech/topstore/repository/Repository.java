package com.apptech.topstore.repository;


import android.content.Context;

import com.apptech.topstore.R;
import com.apptech.topstore.models.login.LoginModel;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.NetworkConnection;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository implements ApiCall {

    private Context mContext;
    public Repository(Context context) {
        this.mContext = context;

    }


    //region Login & Registration
    @Override
    public void validatePhone(String phoneNumber, ValidatePhoneResponse validatePhoneResponse) {

        JSONObject finalObject = new JSONObject();
        try {
            finalObject.put("phone", phoneNumber);
        } catch (JSONException f){
            f.printStackTrace();
        }

        JsonParser jsonParser = new JsonParser();
        JsonObject finalJson = (JsonObject)jsonParser.parse(finalObject.toString());

        MyLog.LogDebugging("API - Validate Phone | INPUT --> USER PHONE NUM : " + phoneNumber);
        MyLog.LogDebugging("API - Validate Phone | INPUT --> JSON INPUT : " + finalObject);

        if (NetworkConnection.NetworkAvailable(mContext)){
            RetrofitConfig retofitConfig = new RetrofitConfig();
            ApiInterfaces apiCall = retofitConfig.getConfig().create(ApiInterfaces.class);
            MyLog.LogDebugging("API - Validate Phone : " + retofitConfig.getConfig().baseUrl()+"phone/request/");
            apiCall.validatePhone(finalJson).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    MyLog.LogDebugging("API - Validate Phone : RESULTS --> " + response.code());
                    if (response.code() == 204){
                        validatePhoneResponse.onResponse();
                    } else {
                        validatePhoneResponse.onFailure();
                        MyToast.error(mContext.getString(R.string.server_error) + " - " + response.code()).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    MyLog.LogDebugging("API - Validate Phone : " + "ANDROID FAILURE");
//                    ToastCarfix.toast(mContext.getString(R.string.android_error), Toast.LENGTH_SHORT, ContextCompat.getColor(mContext, R.color.red300)).show();
                    validatePhoneResponse.onFailure();
                }
            });
        } else {
            MyLog.LogDebugging("API - Validate Phone : " + "NO NETWORK");
            validatePhoneResponse.onNoNetwork();
//            NoNetIntent.open(mContext);
        }
    }

    @Override
    public void login(String phoneNumber, String smsCode, loginResponse loginResponse) {
        JSONObject finalObject = new JSONObject();
        try {
            finalObject.put("phone", phoneNumber);
            finalObject.put("code", smsCode);
        } catch (JSONException f){
            f.printStackTrace();
        }

        JsonParser jsonParser = new JsonParser();
        JsonObject finalJson = (JsonObject)jsonParser.parse(finalObject.toString());

        MyLog.LogDebugging("API - LOGIN | INPUT --> JSON INPUT : " + finalObject);
        if (NetworkConnection.NetworkAvailable(mContext)){
            RetrofitConfig retofitConfig = new RetrofitConfig();
            ApiInterfaces apiCall = retofitConfig.getConfig().create(ApiInterfaces.class);
            MyLog.LogDebugging("API - LOGIN | INPUT --> USER PHONE NUM : " + phoneNumber);
            MyLog.LogDebugging("API - LOGIN | INPUT --> USER SMS CODE : " + smsCode);
            MyLog.LogDebugging("API - LOGIN : " + retofitConfig.getConfig().baseUrl()+"phone/verify/");
            apiCall.login(finalJson).enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    MyLog.LogDebugging("API - LOGIN : RESULTS --> " + response.code());
                    if (response.code() == 200){
                        MySharedPreferences.saveString(MyPreferences.USER_PHONE, phoneNumber);
                        MySharedPreferences.saveString(MyPreferences.USER_TOKEN, response.body().getToken());
                        MyLog.LogDebugging("API - LOGIN : RESULTS --> TOKEN : " + response.body().getToken());
                        loginResponse.onResponse();
                    } else {
                        loginResponse.onFailure();
                        MyToast.error(mContext.getString(R.string.server_error) + " - " + response.code()).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    MyLog.LogDebugging("API - LOGIN : " + "ANDROID FAILURE");
//                    ToastCarfix.toast(mContext.getString(R.string.android_error), Toast.LENGTH_SHORT, ContextCompat.getColor(mContext, R.color.red300)).show();
                    loginResponse.onFailure();
                }
            });
        } else {
            MyLog.LogDebugging("API - LOGIN : " + "NO NETWORK");
            loginResponse.onNoNetwork();
//            NoNetIntent.open(mContext);
        }
    }


    //endregion

}
