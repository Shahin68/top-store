package com.apptech.topstore.repository;


import com.apptech.topstore.models.login.LoginModel;
import com.google.gson.JsonObject;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterfaces {


    //region ---- ------ ---- API REGISTER & VALIDATION --- --- ----- ---
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("phone/request/")
    Call<JsonObject> validatePhone(
            @Body JsonObject phone
    );


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("phone/verify/")
    Call<LoginModel> login(
            @Body JsonObject phone
    );
    //endregion


}
