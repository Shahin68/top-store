package com.apptech.topstore.repository;


public interface ApiCall {


    //region --- Methods - Validate Phone Number
    interface ValidatePhoneResponse{
        void onResponse(); // return api response
        void onFailure();
        void onNoNetwork();
    }
    void validatePhone(String phoneNumber, ValidatePhoneResponse validatePhoneResponse);
    //endregion

    //region --- Methods - Login User - After First Time Registration
    interface loginResponse{
        void onResponse(); // return api response
        void onFailure();
        void onNoNetwork();
    }
    void login(String phoneNumber, String smsCode, loginResponse loginResponse);
    //endregion


}
