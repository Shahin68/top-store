package com.apptech.topstore.repository;

import com.apptech.topstore.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
* class returns item_services Retrofit Built from @param ApiDomain.BASE_URL
* and converts response to item_services JSON Objects
*
* Retrofit Configurations for our api client
* @return RetrofitConfig
* */
public class RetrofitConfig {

    public Retrofit getConfig(){
        return new Retrofit.Builder().client(httpClient())
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    private OkHttpClient httpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

}
