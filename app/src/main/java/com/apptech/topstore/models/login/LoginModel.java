package com.apptech.topstore.models.login;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
