package com.apptech.topstore.database.soldProducts;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.Nullable;

@Entity(tableName = "soldProducts")
public class SoldProductModel {
    @PrimaryKey
    @NonNull
    public String soldID;
    @ColumnInfo(name = "productID")
    public String productID;
    @ColumnInfo(name = "factorID")
    public String factorID;
    @ColumnInfo(name = "soldBarcode")
    private String soldBarcode;
    @ColumnInfo(name = "soldName")
    private String soldName;
    @ColumnInfo(name = "soldDescription")
    private String soldDescription;
    @ColumnInfo(name = "soldBasePrice")
    private int soldBasePrice;
    @ColumnInfo(name = "soldSellingPrice")
    private int soldSellingPrice;
    @ColumnInfo(name = "soldUnit")
    private String soldUnit;
    @ColumnInfo(name = "soldAmount")
    private int soldAmount;
    @ColumnInfo(name = "stockAmount")
    private int stockAmount;
    @ColumnInfo(name = "soldGroupID")
    private int soldGroupID;
    @ColumnInfo(name = "modified")
    @Nullable
    private boolean modified;


    public SoldProductModel(@NonNull String soldID, String productID, String factorID, String soldBarcode, String soldName, String soldDescription, int soldBasePrice, int soldSellingPrice, String soldUnit, int soldAmount, int stockAmount, int soldGroupID, boolean modified) {
        this.soldID = soldID;
        this.productID = productID;
        this.factorID = factorID;
        this.soldBarcode = soldBarcode;
        this.soldName = soldName;
        this.soldDescription = soldDescription;
        this.soldBasePrice = soldBasePrice;
        this.soldSellingPrice = soldSellingPrice;
        this.soldUnit = soldUnit;
        this.soldAmount = soldAmount;
        this.stockAmount = stockAmount;
        this.soldGroupID = soldGroupID;
        this.modified = modified;
    }

    @NonNull
    public String getSoldID() {
        return soldID;
    }

    public void setSoldID(@NonNull String soldID) {
        this.soldID = soldID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getFactorID() {
        return factorID;
    }

    public void setFactorID(String factorID) {
        this.factorID = factorID;
    }

    public String getSoldBarcode() {
        return soldBarcode;
    }

    public void setSoldBarcode(String soldBarcode) {
        this.soldBarcode = soldBarcode;
    }

    public String getSoldName() {
        return soldName;
    }

    public void setSoldName(String soldName) {
        this.soldName = soldName;
    }

    public String getSoldDescription() {
        return soldDescription;
    }

    public void setSoldDescription(String soldDescription) {
        this.soldDescription = soldDescription;
    }

    public int getSoldBasePrice() {
        return soldBasePrice;
    }

    public void setSoldBasePrice(int soldBasePrice) {
        this.soldBasePrice = soldBasePrice;
    }

    public int getSoldSellingPrice() {
        return soldSellingPrice;
    }

    public void setSoldSellingPrice(int soldSellingPrice) {
        this.soldSellingPrice = soldSellingPrice;
    }

    public String getSoldUnit() {
        return soldUnit;
    }

    public void setSoldUnit(String soldUnit) {
        this.soldUnit = soldUnit;
    }

    public int getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(int soldAmount) {
        this.soldAmount = soldAmount;
    }

    public int getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(int stockAmount) {
        this.stockAmount = stockAmount;
    }

    public int getSoldGroupID() {
        return soldGroupID;
    }

    public void setSoldGroupID(int soldGroupID) {
        this.soldGroupID = soldGroupID;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }
}
