package com.apptech.topstore.database.products;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;


@Dao
public interface  ProductsDao {
    @Query("SELECT * FROM products")
    List<ProductModel> getAll();

    @Query("SELECT * FROM products WHERE productID IN (:productIds)")
    List<ProductModel> loadAllByIds(String[] productIds);

    @Query("SELECT * FROM products WHERE productID LIKE :productID LIMIT 1")
    LiveData<ProductModel> findProductByID(String productID);

    @Query("SELECT * FROM products WHERE productName LIKE :productName LIMIT 1")
    ProductModel findByName(String productName);

    @Query("SELECT * FROM products WHERE productBarcode LIKE :productBarcode LIMIT 1")
    ProductModel findByBarcode(int productBarcode);

    @Query("SELECT * FROM products WHERE modified LIKE :modified")
    LiveData<List<ProductModel>> loadAllModified(boolean modified);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<ProductModel> productModels);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOne(ProductModel productModels);

    @Delete
    void delete(ProductModel productModel);

    @Query("SELECT * from products ORDER BY productName DESC")
    LiveData<List<ProductModel>> getAllRegardless();

    @Query("SELECT * from products WHERE modified LIKE :state ORDER BY productID DESC")
    LiveData<List<ProductModel>> getAllLive(boolean state);


    // --- Sorting Options
    @Query("SELECT * from products WHERE modified LIKE :state ORDER BY productSellingPrice DESC")
    LiveData<List<ProductModel>> getAllLiveMostExpensive(boolean state);
    @Query("SELECT * from products WHERE modified LIKE :state ORDER BY productSellingPrice ASC")
    LiveData<List<ProductModel>> getAllLiveLeastExpensive(boolean state);
    @Query("SELECT * from products WHERE modified LIKE :state ORDER BY productAmount DESC")
    LiveData<List<ProductModel>> getAllLiveMostAmount(boolean state);
    @Query("SELECT * from products WHERE modified LIKE :state ORDER BY productAmount ASC")
    LiveData<List<ProductModel>> getAllLiveLeastAmount(boolean state);

    // --- Sorting Options - regardlerss of modified state
    @Query("SELECT * from products ORDER BY productSellingPrice DESC")
    LiveData<List<ProductModel>> getAllLiveMostExpensiveRegardless();
    @Query("SELECT * from products ORDER BY productSellingPrice ASC")
    LiveData<List<ProductModel>> getAllLiveLeastExpensiveRegardless();
    @Query("SELECT * from products ORDER BY productAmount DESC")
    LiveData<List<ProductModel>> getAllLiveMostAmountRegardless();
    @Query("SELECT * from products ORDER BY productAmount ASC")
    LiveData<List<ProductModel>> getAllLiveLeastAmountRegardless();


    // --- Update a Product
    @Update
    void updateOne(ProductModel productModel);

}
