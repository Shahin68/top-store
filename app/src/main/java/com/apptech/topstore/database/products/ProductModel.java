package com.apptech.topstore.database.products;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.List;

import io.reactivex.annotations.Nullable;

@Entity(tableName = "products", indices = {@Index(value = "productBarcode", unique = true)})
public class ProductModel {
    @PrimaryKey
    @NonNull
    private String productID;
    @ColumnInfo(name = "productBarcode")
    @Nullable
    private String productBarcode;
    @ColumnInfo(name = "productName")
    @Nullable
    private String productName;
    @ColumnInfo(name = "productDescription")
    @Nullable
    private String productDescription;
    @ColumnInfo(name = "productBasePrice")
    @Nullable
    private int productBasePrice;
    @ColumnInfo(name = "productSellingPrice")
    @Nullable
    private int productSellingPrice;
    @ColumnInfo(name = "productUnit")
    @Nullable
    private String productUnit;
    @ColumnInfo(name = "productAmount")
    @Nullable
    private int productAmount;
    @ColumnInfo(name = "productGroupIDs")
    @Nullable
    private int productGroupIDs;
    @ColumnInfo(name = "modified")
    @Nullable
    private boolean modified;

    public ProductModel(@NonNull String productID, String productBarcode, String productName, String productDescription, int productBasePrice, int productSellingPrice, String productUnit, int productAmount, int productGroupIDs, boolean modified) {
        this.productID = productID;
        this.productBarcode = productBarcode;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productBasePrice = productBasePrice;
        this.productSellingPrice = productSellingPrice;
        this.productUnit = productUnit;
        this.productAmount = productAmount;
        this.productGroupIDs = productGroupIDs;
        this.modified = modified;
    }

    @NonNull
    public String getProductID() {
        return productID;
    }

    public void setProductID(@NonNull String productID) {
        this.productID = productID;
    }

    public String getProductBarcode() {
        return productBarcode;
    }

    public void setProductBarcode(String productBarcode) {
        this.productBarcode = productBarcode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductBasePrice() {
        return productBasePrice;
    }

    public void setProductBasePrice(int productBasePrice) {
        this.productBasePrice = productBasePrice;
    }

    public int getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(int productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public int getProductGroupIDs() {
        return productGroupIDs;
    }

    public void setProductGroupIDs(int productGroupIDs) {
        this.productGroupIDs = productGroupIDs;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }
}
