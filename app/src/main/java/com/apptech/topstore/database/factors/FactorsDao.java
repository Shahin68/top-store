package com.apptech.topstore.database.factors;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface FactorsDao {
    @Query("SELECT * FROM factors")
    List<FactorModel> getAllFactors();

    @Query("SELECT * FROM factors WHERE factorID IN (:factorIDs)")
    List<FactorModel> loadAllFactorsByIds(String[] factorIDs);

    @Query("SELECT * FROM factors WHERE factorType IN (:type)")
    LiveData<List<FactorModel>> getAllWithType(int type);

    @Query("SELECT * FROM factors WHERE factorReferenceCode LIKE :referenceCode LIMIT 1")
    FactorModel findFactorByReferenceCode(String referenceCode);

    @Query("SELECT * FROM factors WHERE factorID LIKE :factorID LIMIT 1")
    LiveData<FactorModel> findFactorByID(String factorID);

    @Query("SELECT * FROM factors WHERE factorUserID LIKE :userID LIMIT 1")
    FactorModel findFactorByUserID(int userID);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFactorAll(List<FactorModel> factorModels);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOneFactor(FactorModel factorModel);

    @Delete
    void deleteFactor(FactorModel factorModel);

    @Query("SELECT * from factors ORDER BY factorDateCreated DESC")
    LiveData<List<FactorModel>> getAllFactorsLive();


    @Update
    void updateOne(FactorModel factorModel);


    // --- Sorting Options
    @Query("SELECT * from factors ORDER BY factorDateCreated DESC")
    LiveData<List<FactorModel>> getFactorsNewest();
    @Query("SELECT * from factors ORDER BY factorDateCreated ASC")
    LiveData<List<FactorModel>> getFactorsOldest();
    @Query("SELECT * from factors ORDER BY factorCost DESC")
    LiveData<List<FactorModel>> getFactorsMostExpensive();
    @Query("SELECT * from factors ORDER BY factorCost ASC")
    LiveData<List<FactorModel>> getFactorsLeastExpensive();
}
