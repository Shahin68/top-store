package com.apptech.topstore.ui.activities.storage.contents;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.createFactor.SoldProducts;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.ui.activities.storage.storageData.StorageAdapterSelectable;
import com.apptech.topstore.ui.activities.storage.storageData.StorageAdapterSwipeToDeleteCallback;
import com.apptech.topstore.ui.activities.storage.StorageViewModel;
import com.apptech.topstore.ui.activities.storage.storageData.StorageAdapter;
import com.apptech.topstore.utils.FilterPopup;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class StorageContent {
    StorageViewModel mViewModel;
    List<ProductModel> mProductModels;
    InputMethodManager inputMethodManager;
    public List<ProductModel> mSelectedProducts;
    private boolean mFiltered = false;

    // --- toolbar views
    @BindView(R.id.storageTitle) TextView mTitle;
    @BindView(R.id.storageBack) LinearLayout mBack;
    @BindView(R.id.storageTopDecor) AppCompatImageView mTopDecor;

    // --- list
    @BindView(R.id.storage_list) RecyclerView mList;

    // --- Search Views
    @BindView(R.id.storageSearchInput) TextInputEditText mSearchInput;
    @BindView(R.id.storageSearchIcon) AppCompatImageView mSearchIcon;
    @BindView(R.id.storageSearchFilter) AppCompatImageView mFilterButton; // Clickable

    // --- choose products button
    @BindView(R.id.storageAddBtn) Button mChooseButton;


    public StorageContent(Activity activity, StorageViewModel storageViewModel, int type){
        this.mViewModel = storageViewModel;
        ButterKnife.bind(this, activity);
        inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));
        mSearchInput.setTypeface(Fonts.BOLD(activity));
        mChooseButton.setTypeface(Fonts.BOLD(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mSearchIcon.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
            mFilterButton.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
            mChooseButton.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        if (type == 2){ // selectable
            mChooseButton.setVisibility(View.VISIBLE);
            mViewModel.getAllRegardless().observe(StorageActivity.getInstance(),productModels -> {
                initFactorsListSelectable(activity, productModels, false);
            });
        } else { // surfer
            mChooseButton.setVisibility(View.GONE);
            mViewModel.getProductsByModifiedState(true).observe(StorageActivity.getInstance(), productModels -> {
                initFactorsList(activity, productModels, false);
            });
        }

        mFilterButton.setOnClickListener(view -> {
            FilterPopup.getProductFilters(activity, mFilterButton).setOnMenuItemClickListener(item -> {
                if (item.getTitle().toString().equalsIgnoreCase(activity.getString(R.string.remove_filter))){
                    mFilterButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_filter));
                } else {
                    mFilterButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_filtered));
                }
                setFilter(activity, item.getItemId(), type);
                return false;
            });
        });
        setSearchInputMethod(activity, type);
        mSearchIcon.setOnClickListener(v -> searchIconClick(activity, type));
        searchInputTextWatcher(activity, type);

        mSelectedProducts = new ArrayList<>();
        mChooseButton.setOnClickListener(v -> {
            if (type == 2){
                finishActivity(activity, mSelectedProducts);
            } else {
                // nothing to do
            }
        });
    }
    public void initFactorsList(Context context, List<ProductModel> productModels, boolean filtered){
        mProductModels = productModels;

        StorageAdapter adapter = new StorageAdapter(context, productModels, "", mViewModel, filtered);
        mList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mList);
        adapter.notifyDataSetChanged();
        mList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
    }
    public void updateFactorsList(Context context, List<ProductModel> productModels, boolean filtered){
        StorageAdapter adapter = new StorageAdapter(context, productModels, "", mViewModel, filtered);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mList);
        mList.swapAdapter(adapter, false);
    }
    private void search(Context context, String searchPhrase) {
        MyLog.LogDebugging("StorageActivity() ---> " + "StorageContent() ---> " + "search(" + searchPhrase + ")");
        if (searchPhrase.isEmpty()){
            StorageAdapter adapter = new StorageAdapter(context, mProductModels, "", mViewModel, mFiltered);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mSearchInput.getText().clear();
        } else if (!searchPhrase.isEmpty()){
            StorageAdapter adapter = new StorageAdapter(context, mProductModels, searchPhrase, mViewModel, mFiltered);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mList.smoothScrollToPosition(0);
        }
    }
    public void initFactorsListSelectable(Context context, List<ProductModel> productModels, boolean filtered){
        mProductModels = productModels;

        StorageAdapterSelectable adapter = new StorageAdapterSelectable(context, productModels, "", this, mViewModel, filtered);
        mList.setAdapter(adapter);
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSelectableSwipeToDeleteCallback(adapter));
//        itemTouchHelper.attachToRecyclerView(mList);
        adapter.notifyDataSetChanged();
        mList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
    }
    public void updateFactorsListSelectable(Context context, List<ProductModel> productModels, boolean filtered){
        StorageAdapterSelectable adapter = new StorageAdapterSelectable(context, productModels, "", this, mViewModel, filtered);
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSelectableSwipeToDeleteCallback(adapter));
//        itemTouchHelper.attachToRecyclerView(mList);
        mList.swapAdapter(adapter, false);
    }
    private void searchSelectable(Context context, String searchPhrase) {
        MyLog.LogDebugging("StorageActivity() ---> " + "StorageContent() ---> " + "search(" + searchPhrase + ")");
        if (searchPhrase.isEmpty()){
            StorageAdapterSelectable adapter = new StorageAdapterSelectable(context, mProductModels, "", this, mViewModel, mFiltered);
//            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSelectableSwipeToDeleteCallback(adapter));
//            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mSearchInput.getText().clear();
        } else if (!searchPhrase.isEmpty()){
            StorageAdapterSelectable adapter = new StorageAdapterSelectable(context, mProductModels, searchPhrase, this, mViewModel, mFiltered);
//            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new StorageAdapterSelectableSwipeToDeleteCallback(adapter));
//            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mList.smoothScrollToPosition(0);
        }
    }
    private void setFilter(Activity activity, int filterType, int listType) {
        if (listType == 2){
            mViewModel.getAllRegardless(filterType).observe(StorageActivity.getInstance(), productModels -> {
                mList.removeAllViews();
                mFiltered = true;
                updateFactorsListSelectable(activity, productModels, true);
            });
        } else {
            mViewModel.getAllSortedProducts(true, filterType).observe(StorageActivity.getInstance(), productModels -> {
                mList.removeAllViews();
                mFiltered = true;
                initFactorsList(activity, productModels, true);
            });
        }
    }
    private void setSearchInputMethod(Activity activity, int type) {
        mSearchInput.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.KEYCODE_SEARCH) ||
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {

                // Perform action on key press
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    try {
                        inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
                    } catch (NullPointerException f){
                        f.printStackTrace();
                    }
                }
                if (type == 2){
                    searchSelectable(activity, mSearchInput.getText().toString());
                } else {
                    search(activity, mSearchInput.getText().toString());
                }
                return true;
            }
            return false;
        });
    }
    private void searchInputTextWatcher(Activity context, int type) {
        mSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()){
                    if (mSearchIcon.getRotation() != 0) {
                        closeIconToSearchOnly(context);
                        if (type == 2){
                            searchSelectable(context, "");
                        } else {
                            search(context, "");
                        }
                    }
                } else {
                    if (mSearchIcon.getRotation() == 0){
                        searchIconToClose(context);
                    }
                }
            }
        });
    }
    private void searchIconClick(Activity activity, int type) {
        if (mSearchIcon.getRotation() == 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try {
                    // TODO Add if Statement for when keyboard isn't showing to open it
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                } catch (NullPointerException f){
                    f.printStackTrace();
                }
            }
            searchIconToClose(activity);
        } else {
            if (type == 2){
                searchSelectable(activity,"");
            } else {
                search(activity,"");
            }
            closeIconToSearch(activity);
        }
    }

    private void searchIconToClose(Context context) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_close));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 360);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }
    private void closeIconToSearch(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
            } catch (NullPointerException f){
                f.printStackTrace();
            }
        }
    }
    private void closeIconToSearchOnly(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }


    public void finishActivity(Activity context, List<ProductModel> productModels) {
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "finishActivity()");
        if (productModels != null){
            MyToast.good(context.getString(R.string.products_added)).show();
        }
        Intent returnIntent = new Intent();
        SoldProducts.add(productModels);
//        returnIntent.putExtra("sold_product_added", 100);
        context.setResult(1, returnIntent);
        context.finish();
    }
}
