package com.apptech.topstore.ui.activities.createFactor;

import com.apptech.topstore.database.products.ProductModel;

import java.util.List;

public class SoldProducts {
    private static List<ProductModel> list;

    public static List<ProductModel> getList() {
        return list;
    }

    public static void setList(List<ProductModel> list) {
        SoldProducts.list = list;
    }

    public static void add(List<ProductModel> productModels) {
        getList().addAll(productModels);
    }

    public static void clear() {
        try {
            getList().clear();
        } catch (Exception f){
            f.printStackTrace();
        }
    }
}
