package com.apptech.topstore.ui.activities.reports.contents;

public class ReportDataModel {
    float X_VALUE;
    float Y_VALUE;
    String LABEL;

    public ReportDataModel(float x_VALUE, float y_VALUE, String LABEL) {
        X_VALUE = x_VALUE;
        Y_VALUE = y_VALUE;
        this.LABEL = LABEL;
    }

    public float getX_VALUE() {
        return X_VALUE;
    }

    public void setX_VALUE(float x_VALUE) {
        X_VALUE = x_VALUE;
    }

    public float getY_VALUE() {
        return Y_VALUE;
    }

    public void setY_VALUE(float y_VALUE) {
        Y_VALUE = y_VALUE;
    }

    public String getLABEL() {
        return LABEL;
    }

    public void setLABEL(String LABEL) {
        this.LABEL = LABEL;
    }
}
