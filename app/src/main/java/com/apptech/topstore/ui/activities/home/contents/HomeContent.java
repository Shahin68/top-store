package com.apptech.topstore.ui.activities.home.contents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;

import com.apptech.topstore.R;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorActivity;
import com.apptech.topstore.ui.activities.factors.FactorsActivity;
import com.apptech.topstore.ui.activities.newProduct.NewProductActivity;
import com.apptech.topstore.ui.activities.reports.ReportsActivity;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;


import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeContent {

    // ---- Toolbar Views
    @BindView(R.id.cToolbar) CollapsingToolbarLayout mToolbar;
    @BindView(R.id.appbar) AppBarLayout mAppbar;
    @BindView(R.id.nested) NestedScrollView mNested;
    @BindView(R.id.appbar_date_layout) LinearLayout mAppbarDate;
    @BindView(R.id.appbar_image) AppCompatImageView mAppbarImage;
    @BindView(R.id.weekDay) TextView mWeekDay;
    @BindView(R.id.date) TextView mDate;

    // ---- Home Content
    @BindView(R.id.tutorial_backg) AppCompatImageView tutorialBackground;
    @BindView(R.id.tutorial_layout) LinearLayout tutorialLayout;
    @BindView(R.id.stats_card) CardView statsCard;
    @BindView(R.id.returnings_card) CardView returningCard;
    @BindView(R.id.purchases_card) CardView purchasesCard;
    @BindView(R.id.sales_card) CardView salesCard;
    @BindView(R.id.new_product_card) CardView newProductCard;
    @BindView(R.id.storage_card) CardView storageCard;
    @BindView(R.id.factors_card) CardView factorsCard;
    @BindView(R.id.reports_card) CardView reportsCard;
    @BindView(R.id.tutorial_card) RelativeLayout tutorialCard;

    // --- Statistics Views
    @BindView(R.id.stats_purchase_count) TextView mPurchaseCount;
    @BindView(R.id.stats_sales_count) TextView mSalesCount;


    public HomeContent(Activity activity){
        MyLog.LogDebugging("HomeActivity() ---> " + "HomeContent() ---> " + "init");
        ButterKnife.bind(this, activity);
        mAppbarDate.bringToFront();
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mAppbarImage);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(tutorialBackground);
        tutorialLayout.bringToFront();
        mWeekDay.setTypeface(Fonts.BOLD(activity));
        mDate.setTypeface(Fonts.BOLD(activity));
        statsCard.setOnClickListener(statClick(activity));
        returningCard.setOnClickListener(returningClick(activity));
        purchasesCard.setOnClickListener(purchaseClick(activity));
        salesCard.setOnClickListener(salesClick(activity));
        newProductCard.setOnClickListener(newProductClick(activity));
        storageCard.setOnClickListener(storageClick(activity));
        factorsCard.setOnClickListener(factorsClick(activity));
        reportsCard.setOnClickListener(reportsClick(activity));
        tutorialCard.setOnClickListener(tutorialsClick(activity));

        disableAppbarScroll();
        // --- disabling appbar scroll
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            mNested.setNestedScrollingEnabled(false);
        }
    }
    public void setTopMargin(int statusHeight){
        CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) mAppbarImage.getLayoutParams();
        layoutParams.topMargin = -statusHeight;
        mAppbarImage.setLayoutParams(layoutParams);
    }

    public void setStatisticsSales(int salesCount){
        MyLog.LogDebugging("HomeActivity() ---> " + "HomeContent() ---> " + "SALES COUNT = " + salesCount);
        mSalesCount.setText(ConvertNumbers.parseNumbers(String.valueOf(salesCount), false));
    }
    public void setStatisticsPurchases(int purchaseCount){
        MyLog.LogDebugging("HomeActivity() ---> " + "HomeContent() ---> " + "PURCHASE COUNT = " + purchaseCount);
        mPurchaseCount.setText(ConvertNumbers.parseNumbers(String.valueOf(purchaseCount), false));
    }
    public void setTodayDate(String weekday, String date){
        MyLog.LogDebugging("HomeActivity() ---> " + "HomeContent() ---> " + "setTodayDate()");
        mWeekDay.setText(ConvertNumbers.parseNumbers(String.valueOf(weekday), false));
        mDate.setText(ConvertNumbers.parseNumbers(String.valueOf(date), false));
    }


    private View.OnClickListener statClick(Context context){
        return v -> MyToast.good(context.getString(R.string.sales_statistics)).show();
    }
    private View.OnClickListener returningClick(Context context){
        return v -> {
            Intent intent = new Intent(context, CreateFactorActivity.class);
            intent.putExtra("factor_type", 0); // rejected type factor
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener purchaseClick(Context context){
        return v -> {
            Intent intent = new Intent(context, CreateFactorActivity.class);
            intent.putExtra("factor_type", 1); // purchase type factor
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener salesClick(Context context){
        return v -> {
            Intent intent = new Intent(context, CreateFactorActivity.class);
            intent.putExtra("factor_type", 2); // sale type factor
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener newProductClick(Context context){
        return v -> {
            Intent intent = new Intent(context, NewProductActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener storageClick(Context context){
        return v -> {
            Intent intent = new Intent(context, StorageActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener factorsClick(Context context){
        return v -> {
            Intent intent = new Intent(context, FactorsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener reportsClick(Context context){
        return v -> {
            Intent intent = new Intent(context, ReportsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        };
    }
    private View.OnClickListener tutorialsClick(Context context){
        return v -> MyToast.good(context.getString(R.string.tutorial_using_ris)).show();
    }

    private void disableAppbarScroll() {
        mAppbar.setEnabled(false);
        mToolbar.setEnabled(false);

    }

}
