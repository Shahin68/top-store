package com.apptech.topstore.ui.activities.newProduct.contents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.databaseLocal.Unit;
import com.apptech.topstore.ui.activities.createFactor.SoldProducts;
import com.apptech.topstore.ui.activities.newProduct.NewProductViewModel;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewProductContent {
    private NewProductViewModel mViewModel;

    // --- toolbar views
    @BindView(R.id.newProductTitle) TextView mTitle;
    @BindView(R.id.newProductBack) LinearLayout mBack;
    @BindView(R.id.newProductTopDecor) AppCompatImageView mTopDecor;

    // ---- Content Views
    @BindView(R.id.newProductNameInput) TextInputEditText mProductNameInput;
    @BindView(R.id.newProductDefaultPriceInput) TextInputEditText mProductBasePriceInput;
    @BindView(R.id.newProducSellPriceInput) TextInputEditText mProductSellPriceInput;
    @BindView(R.id.newProductBarcodeInput) TextInputEditText mProductBarcodeInput;
    @BindView(R.id.newProductBarcodeImg) AppCompatImageView mProductBarcodeImg;
    @BindView(R.id.newProductUnitTxt) TextView mProductUnitTxt;
    @BindView(R.id.newProductInfoInput) TextInputEditText mProductInfoInput;
    @BindView(R.id.newProductUnit) CardView mChooseUnit;
    @BindView(R.id.newProductAmountInput) TextInputEditText mProductAmountInput;

    @BindView(R.id.newProductDefaultPriceToman) TextView mProductBasePriceToman;
    @BindView(R.id.newProducSellPriceToman) TextView mProductSellPriceToman;

    // --- add new product button
    @BindView(R.id.newProductAddBtn) Button mAddProduct;

    int UNIT_ID = 0;

    public NewProductContent(Activity activity, NewProductViewModel viewModel, int type){
        this.mViewModel = viewModel;
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));

        mProductNameInput.setTypeface(Fonts.BOLD(activity));
        mProductBasePriceInput.setTypeface(Fonts.BOLD(activity));
        mProductSellPriceInput.setTypeface(Fonts.BOLD(activity));
        mProductBarcodeInput.setTypeface(Fonts.BOLD(activity));
        mProductUnitTxt.setTypeface(Fonts.BOLD(activity));
        mProductInfoInput.setTypeface(Fonts.BOLD(activity));
        mProductAmountInput.setTypeface(Fonts.BOLD(activity));
        mProductBasePriceToman.setTypeface(Fonts.BOLD(activity));
        mProductSellPriceToman.setTypeface(Fonts.BOLD(activity));

        mProductBarcodeImg.setBackground(ContextCompat.getDrawable(activity,R.drawable.ripple_round_accent_48dp));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mChooseUnit.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        mChooseUnit.setOnClickListener(v -> showUnitMenu(activity, mChooseUnit));

        mProductBarcodeImg.setOnClickListener(v -> {

        });
        mAddProduct.setOnClickListener(v -> {
            if (mProductBarcodeInput.getText().toString().isEmpty()||
                    mProductNameInput.getText().toString().isEmpty()||
                    mProductInfoInput.getText().toString().isEmpty()||
                    mProductBasePriceInput.getText().toString().isEmpty() ||
                    mProductSellPriceInput.getText().toString().isEmpty() ||
                    mProductAmountInput.getText().toString().isEmpty()) {
                MyToast.warning(activity.getString(R.string.please_fill_all)).show();
            } else {
                if (type == 2){ // creating new product through factor creation in newFactor Activity
                    createProductForType(
                            activity,
                            mProductBarcodeInput.getText().toString(),
                            mProductNameInput.getText().toString(),
                            mProductInfoInput.getText().toString(),
                            Integer.parseInt(mProductBasePriceInput.getText().toString()),
                            Integer.parseInt(mProductSellPriceInput.getText().toString()),
                            Unit.getUnit(activity, UNIT_ID),
                            Integer.valueOf(mProductAmountInput.getText().toString()),
                            1
                    );
                } else { // creating new product through newProduct Activity
                    createProduct(
                            activity,
                            mProductBarcodeInput.getText().toString(),
                            mProductNameInput.getText().toString(),
                            mProductInfoInput.getText().toString(),
                            Integer.parseInt(mProductBasePriceInput.getText().toString()),
                            Integer.parseInt(mProductSellPriceInput.getText().toString()),
                            Unit.getUnit(activity, UNIT_ID),
                            Integer.valueOf(mProductAmountInput.getText().toString()),
                            1
                    );
                }
            }
        });

    }
    private void createProduct(Activity activity, String barcode, String name, String description, int basePrice, int sellingPrice, String unit, int amount, int groupID){
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "createProduct()");
        mViewModel.insert(activity, new ProductModel(
                "abc" + barcode,
                barcode,
                name,
                description,
                basePrice,
                sellingPrice,
                unit,
                amount,
                groupID,
                true
        ), this);
    }
    private void createProductForType(Activity activity, String barcode, String name, String description, int basePrice, int sellingPrice, String unit, int amount, int groupID){
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "createProductForType()");
        mViewModel.insertForType(activity, new ProductModel(
                "abc" + barcode,
                barcode,
                name,
                description,
                basePrice,
                sellingPrice,
                unit,
                amount,
                groupID,
                true
        ), this);
    }
    public void clearEntries(Context context) {
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "clearEntries()");
        Objects.requireNonNull(mProductBarcodeInput.getText()).clear();
        Objects.requireNonNull(mProductNameInput.getText()).clear();
        Objects.requireNonNull(mProductInfoInput.getText()).clear();
        Objects.requireNonNull(mProductBasePriceInput.getText()).clear();
        Objects.requireNonNull(mProductSellPriceInput.getText()).clear();
        Objects.requireNonNull(mProductAmountInput.getText()).clear();
        mProductUnitTxt.setText(context.getString(R.string.choose));
        MyToast.good(context.getString(R.string.product_stored)).show();
    }
    public void finishActivity(Activity context, List<ProductModel> productModels) {
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "finishActivity()");
        MyToast.good(context.getString(R.string.product_stored)).show();
        Intent returnIntent = new Intent();
        SoldProducts.add(productModels);
//        returnIntent.putExtra("sold_product_added", 100);
        context.setResult(1, returnIntent);
        context.finish();
    }
    private void showUnitMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Unit.units(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    Unit.units(activity)[i].second,
                    i,
                    Unit.units(activity)[i].first);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    UNIT_ID = item.getItemId();
                    mProductUnitTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
}
