package com.apptech.topstore.ui.activities.initialSettings.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyPopup;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsInitialContent {
    // --- toolbar views
    @BindView(R.id.settingsInitialTitle) TextView mTitle;

    // --- Contents
    @BindView(R.id.settingsInitialCurrency) CardView mCurrency;
    @BindView(R.id.settingsInitialCurrencyTxt) TextView mCurrencyTxt;
    @BindView(R.id.settingsInitialLanguage) CardView mLanguage;
    @BindView(R.id.settingsInitialLanguageTxt) TextView mLanguageTxt;
    @BindView(R.id.settingsInitialStoreName) TextInputEditText mStoreNameInput;
    @BindView(R.id.settingsInitialUserName) TextInputEditText mUserNameInput;
    @BindView(R.id.settingsInitialStoreBtn) Button mStoreSettings;


    public SettingsInitialContent(Activity activity){
        ButterKnife.bind(this, activity);
        mTitle.setTypeface(Fonts.BOLD(activity));
        mStoreSettings.setTypeface(Fonts.BOLD(activity));
        mCurrencyTxt.setTypeface(Fonts.BOLD(activity));
        mLanguageTxt.setTypeface(Fonts.BOLD(activity));
        mStoreNameInput.setTypeface(Fonts.BOLD(activity));
        mUserNameInput.setTypeface(Fonts.BOLD(activity));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mStoreSettings.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCurrency.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLanguage.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }


        mCurrency.setOnClickListener(v -> {
            showCurrencyMenu(activity, v);
        });
        mLanguage.setOnClickListener(v -> {
            showLanguageMenu(activity, v);
        });
        mStoreSettings.setOnClickListener(v -> {
            if (mStoreNameInput.getText().toString().isEmpty() || mUserNameInput.getText().toString().isEmpty()){
                MyToast.warning(activity.getString(R.string.please_set_all)).show();
            } else {
                MySharedPreferences.saveString(MyPreferences.USER_CURRENCY, mCurrencyTxt.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_LANGUAGE, mLanguageTxt.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_STORE_NAME, mStoreNameInput.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_NAME, mUserNameInput.getText().toString());
                openHomeActivity(activity);
            }
        });
    }

    public void openHomeActivity(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openHomeActivity()");
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }

    private void showCurrencyMenu(Activity activity, View view){
        MyPopup.currencyMenu(activity, view).setOnMenuItemClickListener(item -> {
            mCurrencyTxt.setText(item.getTitle());
            return false;
        });
    }
    private void showLanguageMenu(Activity activity, View view){
        MyPopup.languageMenu(activity, view).setOnMenuItemClickListener(item -> {
            mLanguageTxt.setText(item.getTitle());
            return false;
        });
    }
}
