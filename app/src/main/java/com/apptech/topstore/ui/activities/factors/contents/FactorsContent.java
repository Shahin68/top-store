package com.apptech.topstore.ui.activities.factors.contents;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.ui.activities.factors.FactorsActivity;
import com.apptech.topstore.ui.activities.factors.FactorsViewModel;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsAdapter;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsAdapterSwipeToDeleteCallback;
import com.apptech.topstore.utils.FilterPopup;
import com.apptech.topstore.utils.Fonts;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FactorsContent {
    FactorsViewModel mViewModel;
    List<FactorModel> mFactorModels;
    InputMethodManager inputMethodManager;
    private boolean mFiltered;

    // --- toolbar views
    @BindView(R.id.factorsTitle) TextView mTitle;
    @BindView(R.id.factorsBack) LinearLayout mBack;
    @BindView(R.id.factorsTopDecor) AppCompatImageView mTopDecor;

    // --- list
    @BindView(R.id.factors_list) RecyclerView mList;

    // --- Search Views
    @BindView(R.id.factorsSearchInput) TextInputEditText mSearchInput;
    @BindView(R.id.factorsSearchIcon) AppCompatImageView mSearchIcon;
    @BindView(R.id.factorsSearchFilter) AppCompatImageView mFilterButton; // Clickable


    public FactorsContent(Activity activity, FactorsViewModel factorsViewModel, List<FactorModel> factorModels){
        mViewModel = factorsViewModel;
        ButterKnife.bind(this, activity);
        inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));
        mSearchInput.setTypeface(Fonts.BOLD(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mSearchIcon.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
            mFilterButton.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
        }


        initFactorsList(activity, factorModels, false);

        mFilterButton.setOnClickListener(view -> {
            FilterPopup.getFactorFilters(activity, mFilterButton).setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getTitle().toString().equalsIgnoreCase(activity.getString(R.string.remove_filter))){
                        mFilterButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_filter));
                    } else {
                        mFilterButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_filtered));
                    }
                    setFilter(activity, item.getItemId());
                    return false;
                }
            });
        });
        setSearchInputMethod(activity);
        mSearchIcon.setOnClickListener(v -> searchIconClick(activity));
        searchInputTextWatcher(activity);

    }
    public void initFactorsList(Context context, List<FactorModel> factorModels, boolean filtered){
        mFactorModels = factorModels;

        FactorsAdapter adapter = new FactorsAdapter(context, factorModels, "", mViewModel, filtered);
        mList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mList);
        adapter.notifyDataSetChanged();
        mList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
    }
    public void updateFactorsList(Context context, List<FactorModel> factorModels, boolean filtered){
        FactorsAdapter adapter = new FactorsAdapter(context, factorModels, "", mViewModel, filtered);
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
//        itemTouchHelper.attachToRecyclerView(mList);
        mList.swapAdapter(adapter, false);
    }
    private void search (Context context, String searchPhrase, boolean filtered) {
        if (searchPhrase.isEmpty()){
            FactorsAdapter adapter = new FactorsAdapter(context, mFactorModels, "", mViewModel, filtered);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mSearchInput.getText().clear();
        } else if (!searchPhrase.isEmpty()){
            FactorsAdapter adapter = new FactorsAdapter(context, mFactorModels, searchPhrase, mViewModel, filtered);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mList.smoothScrollToPosition(0);
        }
    }
    private void setFilter(Activity activity, int filterType) {
        mViewModel.getAllRegardless(filterType).observe(FactorsActivity.getInstance(), factorModels -> {
            mList.removeAllViews();
            mFiltered = true;
            updateFactorsList(activity, factorModels, true);
        });
    }
    private void setSearchInputMethod(Activity activity) {
        mSearchInput.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.KEYCODE_SEARCH) ||
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {

                // Perform action on key press
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    try {
                        inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
                    } catch (NullPointerException f){
                        f.printStackTrace();
                    }
                }
                search(activity, mSearchInput.getText().toString(), mFiltered);
                return true;
            }
            return false;
        });
    }
    private void searchInputTextWatcher(Activity context) {
        mSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()){
                    if (mSearchIcon.getRotation() != 0) {
                        closeIconToSearchOnly(context);
                        search(context, "", mFiltered);
                    }
                } else {
                    if (mSearchIcon.getRotation() == 0){
                        searchIconToClose(context);
                    }
                }
            }
        });
    }
    private void searchIconClick(Activity activity) {
        if (mSearchIcon.getRotation() == 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try {
                    // TODO Add if Statement for when keyboard isn't showing to open it
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                } catch (NullPointerException f){
                    f.printStackTrace();
                }
            }
            searchIconToClose(activity);
        } else {
            search(activity,"", mFiltered);
            closeIconToSearch(activity);
        }
    }

    private void searchIconToClose(Context context) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_close));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 360);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }
    private void closeIconToSearch(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
            } catch (NullPointerException f){
                f.printStackTrace();
            }
        }
    }
    private void closeIconToSearchOnly(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }

}
