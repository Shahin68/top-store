package com.apptech.topstore.ui.activities.profile.contents;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyPopup;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileContent {
    // --- toolbar views
    @BindView(R.id.profileTitle) TextView mTitle;
    @BindView(R.id.profileBack) LinearLayout mBack;
    @BindView(R.id.profileTopDecor) AppCompatImageView mTopDecor;
    @BindView(R.id.profileStoreBtn) Button mStoreBtn;

    // --- Contents
    @BindView(R.id.profileStoreNameInput) TextInputEditText mStoreNameInput;
    @BindView(R.id.profileUserNameInput) TextInputEditText mUserNameInput;
    @BindView(R.id.profileStoreNameTxt) TextView mStoreNameTxt;
    @BindView(R.id.profileUserNameTxt) TextView mUserNameTxt;

    @BindView(R.id.profileCountry) CardView mCountryCard;
    @BindView(R.id.profileCountryTxt) TextView mCountryTxt;
    @BindView(R.id.profileCity) CardView mCityCard;
    @BindView(R.id.profileCityTxt) TextView mCityTxt;
    @BindView(R.id.profileLocation) CardView mLocationCard;
    @BindView(R.id.profileLocationTxt) TextView mLocationTxt;
    @BindView(R.id.profileLocationChooseTxt) TextView mLocationChooseTxt;

    // --- phone
    @BindView(R.id.profileCountryCode) LinearLayout mCountryCode; // Clickable
    @BindView(R.id.profileCountryCodeTxt) TextView mCountryCodeTxt;
    @BindView(R.id.profileCountryCodeFlag) AppCompatImageView mCountryFlag;
    @BindView(R.id.profilePhoneInput) TextInputEditText mPhoneInput;

    public ProfileContent(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));
        mStoreBtn.setTypeface(Fonts.BOLD(activity));
        mStoreNameInput.setTypeface(Fonts.BOLD(activity));
        mUserNameInput.setTypeface(Fonts.BOLD(activity));
        mStoreNameTxt.setTypeface(Fonts.BOLD(activity));
        mUserNameTxt.setTypeface(Fonts.BOLD(activity));
        mCountryTxt.setTypeface(Fonts.BOLD(activity));
        mCityTxt.setTypeface(Fonts.BOLD(activity));
        mLocationTxt.setTypeface(Fonts.BOLD(activity));
        mLocationChooseTxt.setTypeface(Fonts.BOLD(activity));
        mPhoneInput.setTypeface(Fonts.BOLD(activity));
        mCountryCodeTxt.setTypeface(Fonts.BOLD(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mStoreBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCountryCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCityCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLocationCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }


        mStoreNameInput.setText(MySharedPreferences.getString(MyPreferences.USER_STORE_NAME, ""));
        mUserNameInput.setText(MySharedPreferences.getString(MyPreferences.USER_NAME, ""));
        mPhoneInput.setText(MySharedPreferences.getString(MyPreferences.USER_PHONE, ""));
        mCountryCodeTxt.setText(ConvertNumbers.parseNumbers(MySharedPreferences.getString(MyPreferences.USER_COUNTRY_CODE, activity.getString(R.string.code)), false));
        mCountryTxt.setText(MySharedPreferences.getString(MyPreferences.USER_COUNTRY, activity.getString(R.string.country)));
        if (MySharedPreferences.getInteger(MyPreferences.USER_COUNTRY_FLAG, -1) == -1){
            mCountryFlag.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_flag));
        } else {
            mCountryFlag.setImageDrawable(ContextCompat.getDrawable(activity, Country.countryFlags()[MySharedPreferences.getInteger(MyPreferences.USER_COUNTRY_FLAG, 0)]));
        }




        mCountryCard.setOnClickListener(v -> {
            showCountryMenu(activity, mCountryCard);
        });
        mCityCard.setOnClickListener(v -> {});
        mLocationCard.setOnClickListener(v -> {});
        mCountryCode.setOnClickListener(v -> showCountryCodeMenu(activity, mCountryCode));


        mStoreBtn.setOnClickListener(v -> {
            if (mPhoneInput.getText().toString().isEmpty() ||
                    mCountryCodeTxt.getText().toString().equalsIgnoreCase(activity.getString(R.string.code))  ||
                    mStoreNameInput.getText().toString().isEmpty() ||
                    mUserNameInput.getText().toString().isEmpty() ||
                    mCountryTxt.getText().toString().equalsIgnoreCase(activity.getString(R.string.choose))
            ){
                MyToast.warning(activity.getString(R.string.fill_fields_first)).show();
            } else {
                MySharedPreferences.saveString(MyPreferences.USER_PHONE, mPhoneInput.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_STORE_NAME, mStoreNameInput.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_NAME, mUserNameInput.getText().toString());
                MySharedPreferences.saveString(MyPreferences.USER_COUNTRY, mCountryTxt.getText().toString());
                activity.finish();
            }
        });

    }
    private void showCountryCodeMenu(Activity activity, View view){
        MyPopup.countryCodeMenu(activity, view).setOnMenuItemClickListener((position, item) -> {
            mCountryFlag.setImageDrawable(ContextCompat.getDrawable(activity, item.getIcon()));
            MySharedPreferences.saveInteger(MyPreferences.USER_COUNTRY_FLAG, (Integer) item.getTag());
            mCountryCodeTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_COUNTRY_CODE, Country.countryNames()[(Integer) item.getTag()].second);
        });
    }
    private void showCountryMenu(Activity activity, View view){
        MyPopup.countryNameMenu(activity, view).setOnMenuItemClickListener((position, item) -> {
            mCountryTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_COUNTRY, item.getTitle());
        });
    }

}
