package com.apptech.topstore.ui.activities.settings.contents;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyPopup;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsContent {
    // --- toolbar views
    @BindView(R.id.settingsTitle) TextView mTitle;
    @BindView(R.id.settingsBack) LinearLayout mBack;
    @BindView(R.id.settingsTopDecor) AppCompatImageView mTopDecor;

    // --- Content Views
    @BindView(R.id.settingsCurrency) CardView mCurrency;
    @BindView(R.id.settingsCalendar) CardView mCalendar;
    @BindView(R.id.settingsLanguage) CardView mLanguage;
    @BindView(R.id.settingsNotifications) CardView mNotifications;
    @BindView(R.id.settingsFactors) CardView mFactors;

    @BindView(R.id.settingsCurrencyTxt) TextView mCurrencyTxt;
    @BindView(R.id.settingsCalendarTxt) TextView mCalendarTxt;
    @BindView(R.id.settingsLanguageTxt) TextView mLanguageTxt;
    @BindView(R.id.settingsNotificationsTxt) TextView mNotificationTxt;

    public SettingsContent(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));
        mCurrencyTxt.setTypeface(Fonts.BOLD(activity));
        mCalendarTxt.setTypeface(Fonts.BOLD(activity));
        mLanguageTxt.setTypeface(Fonts.BOLD(activity));
        mNotificationTxt.setTypeface(Fonts.BOLD(activity));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCurrency.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCalendar.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLanguage.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mNotifications.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mFactors.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }

        mCurrencyTxt.setText(MySharedPreferences.getString(MyPreferences.USER_CURRENCY, activity.getString(R.string.currency)));
        mCalendarTxt.setText(MySharedPreferences.getString(MyPreferences.USER_CALENDAR_TYPE, activity.getString(R.string.calendar)));
        mLanguageTxt.setText(MySharedPreferences.getString(MyPreferences.USER_LANGUAGE, activity.getString(R.string.language)));
        mNotificationTxt.setText(MySharedPreferences.getString(MyPreferences.USER_ACTIVE_NOTIFICATION, activity.getString(R.string.activated)));


        mCurrency.setOnClickListener(v -> {
            showCurrencyMenu(activity, v);
        });
        mCalendar.setOnClickListener(v -> {
            showCalendarMenu(activity, v);
        });
        mLanguage.setOnClickListener(v -> {
            showLanguageMenu(activity, v);
        });
        mNotifications.setOnClickListener(v -> {
            showStateMenu(activity, v);
        });
    }

    private void showCurrencyMenu(Activity activity, View view){
        MyPopup.currencyMenu(activity, view).setOnMenuItemClickListener(item -> {
            mCurrencyTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_CURRENCY, item.getTitle().toString());
            return false;
        });
    }
    private void showCalendarMenu(Activity activity, View view){
        MyPopup.calendarMenu(activity, view).setOnMenuItemClickListener(item -> {
            mCalendarTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_CALENDAR_TYPE, item.getTitle().toString());
            return false;
        });
    }
    private void showLanguageMenu(Activity activity, View view){
        MyPopup.languageMenu(activity, view).setOnMenuItemClickListener(item -> {
            mLanguageTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_LANGUAGE, item.getTitle().toString());
            return false;
        });
    }
    private void showStateMenu(Activity activity, View view){
        MyPopup.stateMenu(activity, view).setOnMenuItemClickListener(item -> {
            mNotificationTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_ACTIVE_NOTIFICATION, item.getTitle().toString());
            return false;
        });
    }
}
