package com.apptech.topstore.ui.activities.login.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.repository.ApiCall;
import com.apptech.topstore.repository.Repository;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.ui.activities.initialSettings.SettingsInitialActivity;
import com.apptech.topstore.ui.activities.login.LoginActivity;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyPopup;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.bumptech.glide.Glide;
import com.chaos.view.PinView;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginContents {
    CountDownTimer Count;
    private boolean COUNTER_STATUS = false;

    @BindView(R.id.login_logo)
    AppCompatImageView mLogo;
    @BindView(R.id.login_txt)
    TextView mTxt;
    @BindView(R.id.login_button)
    Button mButton;
    @BindView(R.id.login_phone)
    CardView mPhoneView;
    @BindView(R.id.login_pinview)
    LinearLayout mPinLayout;

    // --- phone & Pin views
    @BindView(R.id.country_code)
    LinearLayout mCountryCode; // Clickable
    @BindView(R.id.country_flag)
    AppCompatImageView mCountryFlag;
    @BindView(R.id.phone_txt)
    TextView mPhoneTxt;
    @BindView(R.id.phone_input)
    TextInputEditText mPhoneInput;
    @BindView(R.id.country_code_txt)
    TextView mCountryCodeTxt; // Clickable
    @BindView(R.id.pin_edit_phone)
    TextView mEditPhone;
    @BindView(R.id.pin_remaining_time)
    TextView mPinRetryTime;
    @BindView(R.id.pinView)
    PinView mPinView;
    public LoginContents(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.logo_blue).into(mLogo);
        mPinView.setAnimationEnable(true);
        mTxt.setTypeface(Fonts.BOLD(activity));
        mPhoneTxt.setTypeface(Fonts.BOLD(activity));
        mCountryCodeTxt.setTypeface(Fonts.BOLD(activity));
        mPhoneInput.setTypeface(Fonts.BOLD(activity));
        mButton.setTypeface(Fonts.BOLD(activity));
        mPinRetryTime.setTypeface(Fonts.BOLD(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mButton.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mEditPhone.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mPinRetryTime.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }

        mPinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





        mCountryCode.setOnClickListener(v -> showCountryMenu(activity, mCountryCode));

        startTimer(activity);
        mEditPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButton.setText(activity.getString(R.string.receive_sms_code));
                mPinLayout.setVisibility(View.GONE);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPhoneView.setVisibility(View.VISIBLE);
                    }
                }, 100);
            }
        });
        mPinRetryTime.setOnClickListener(v -> {
            if (COUNTER_STATUS){
                startTimer(activity);
            }
        });


        // --- main Button

        // ---- TODO Country code is  removed due to server 500 error
        mButton.setOnClickListener(v -> {
            if (mPhoneInput.getText().toString().isEmpty() || mCountryCodeTxt.getText() == activity.getString(R.string.code)){
                MyToast.warning(activity.getString(R.string.enter_valid_phone_number)).show();
            } else {
                if (mPhoneView.getVisibility() == View.VISIBLE){
                    String phoneNumber = "0" + mPhoneInput.getText().toString();

                    new Repository(activity).validatePhone(phoneNumber, new ApiCall.ValidatePhoneResponse() {
                        @Override
                        public void onResponse() {
                            mButton.setText(activity.getString(R.string.enter));
                            mPhoneView.setVisibility(View.GONE);
                            new Handler(Looper.getMainLooper()).postDelayed(() -> mPinLayout.setVisibility(View.VISIBLE), 100);
                        }

                        @Override
                        public void onFailure() {

                        }

                        @Override
                        public void onNoNetwork() {

                        }
                    });
                } else {

                    String phoneNumber = "0" + mPhoneInput.getText().toString();

                    if (mPinView.length() < 5){
                        MyToast.warning(activity.getString(R.string.enter_pin_first)).show();
                    } else {
                        new Repository(activity).login(phoneNumber, mPinView.getText().toString(), new ApiCall.loginResponse() {
                            @Override
                            public void onResponse() {
                                MySharedPreferences.saveString(MyPreferences.USER_PHONE, mPhoneInput.getText().toString());
                                MySharedPreferences.saveString(MyPreferences.USER_COUNTRY_CODE, mCountryCodeTxt.getText().toString());
                                openInitialSettings(activity);
                            }

                            @Override
                            public void onFailure() {

                            }

                            @Override
                            public void onNoNetwork() {

                            }
                        });
                    }


                }
            }
//            openInitialSettings(activity); // TODO Remove This Later - this is to bypass api
        });
    }
    private void startTimer(Activity activity){
        // --- Timing Process
        Count = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                mPinRetryTime.setText(ConvertNumbers.parseNumbers(String.valueOf(millisUntilFinished / 1000),false) + " " + activity.getString(R.string.second));
                mPinRetryTime.setTextColor(ContextCompat.getColor(activity,R.color.textDark));
                COUNTER_STATUS = false;
            }

            public void onFinish() {
                mPinRetryTime.setText(activity.getString(R.string.receive_smscode_again));
                mPinRetryTime.setTextColor(ContextCompat.getColor(activity,R.color.accent));
                COUNTER_STATUS = true;
            }
        };
        Count.start();
    }
    public void openInitialSettings(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openInitialSettings()");
        Intent intent = new Intent(activity, SettingsInitialActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }
    public void openHomeActivity(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openHomeActivity()");
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }

    public void onBackPresses(LoginActivity activity){
        if (mPhoneView.getVisibility() != View.VISIBLE){
            mButton.setText(activity.getString(R.string.receive_sms_code));
            mPinLayout.setVisibility(View.GONE);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPhoneView.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            activity.pressedBack();
        }
    }

    private void showCountryMenu(Activity activity, View view){
        MyPopup.countryCodeMenu(activity, view).setOnMenuItemClickListener((position, item) -> {
            mCountryFlag.setImageDrawable(ContextCompat.getDrawable(activity, item.getIcon()));
            MySharedPreferences.saveInteger(MyPreferences.USER_COUNTRY_FLAG, (Integer) item.getTag());
            mCountryCodeTxt.setText(item.getTitle());
            MySharedPreferences.saveString(MyPreferences.USER_COUNTRY_CODE, Country.countryNames()[(Integer) item.getTag()].second);
        });
    }

}
