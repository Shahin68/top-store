package com.apptech.topstore.ui.activities.customerName.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyPopup;
import com.apptech.topstore.utils.MyToast;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerNameContent {
    // --- toolbar views
    @BindView(R.id.customerNameMessage) TextView mMessage;
    @BindView(R.id.customerNameBtn) Button mButton;

    // --- About Us Contents
    @BindView(R.id.customerNameInput) TextInputEditText mNameInput;


    // --- phone
    @BindView(R.id.customerNameCountryCode) LinearLayout mCountryCode; // Clickable
    @BindView(R.id.customerNameCountryCodeTxt) TextView mCountryCodeTxt;
    @BindView(R.id.customerNameCountryCodeFlag) AppCompatImageView mCountryFlag;
    @BindView(R.id.customerNamePhoneInput) TextInputEditText mPhoneInput;


    public CustomerNameContent(Activity activity){
        ButterKnife.bind(this, activity);
        mMessage.setTypeface(Fonts.BOLD(activity));
        mNameInput.setTypeface(Fonts.BOLD(activity));
        mPhoneInput.setTypeface(Fonts.BOLD(activity));
        mCountryCodeTxt.setTypeface(Fonts.BOLD(activity));
        mButton.setTypeface(Fonts.BOLD(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mButton.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }


        mCountryCode.setOnClickListener(v -> {
            showCountryMenu(activity, mCountryCode);
        });
        mButton.setOnClickListener(v -> {
            if (mNameInput.getText().toString().isEmpty() ||
                    mPhoneInput.getText().toString().isEmpty() ||
                    mCountryCodeTxt.getText() == activity.getString(R.string.code)){
                MyToast.warning(activity.getString(R.string.fill_fields_first)).show();
            } else {
                finishActivity(activity,
                        mNameInput.getText().toString(),
                        mCountryCodeTxt.getText() + mPhoneInput.getText().toString()
                );
            }
        });
    }
    public void finishActivity(Activity context, String customerName, String phone) {
        MyLog.LogDebugging("CustomerName() ---> " + "NewProductContent() ---> " + "finishActivity()");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("customer_name", customerName);
        returnIntent.putExtra("customer_phone", phone);
        context.setResult(1, returnIntent);
        context.finish();
    }

    private void showCountryMenu(Activity activity, View view){
        MyPopup.countryCodeMenu(activity, view).setOnMenuItemClickListener((position, item) -> {
            mCountryFlag.setImageDrawable(ContextCompat.getDrawable(activity, item.getIcon()));
            mCountryCodeTxt.setText(item.getTitle());
        });
    }


}
