package com.apptech.topstore.ui.activities.createFactor;

import android.app.Activity;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.R;
import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.createFactor.contents.CreateFactorContent;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CreateFactorViewModel extends ViewModel {
    @Inject
    AppDatabase appDatabase;
    Disposable disposable;


    public LiveData<List<FactorModel>> getFactors() {
        return appDatabase.factorsDao().getAllFactorsLive();
    }

    public LiveData<ProductModel> getProducts(String id) {
        return appDatabase.productsDao().findProductByID(id);
    }

    public void delete(SoldProductModel soldProductModel){
        new deleteTask().execute(soldProductModel);
    }
    private class deleteTask extends AsyncTask<SoldProductModel, Void, Void>{
        @Override
        protected Void doInBackground(SoldProductModel... soldProductModels) {
            appDatabase.soldProductDao().delete(soldProductModels[0]);
            appDatabase.productsDao().updateOne(new ProductModel(
                    soldProductModels[0].getProductID(),
                    soldProductModels[0].getSoldBarcode(),
                    soldProductModels[0].getSoldName(),
                    soldProductModels[0].getSoldDescription(),
                    soldProductModels[0].getSoldBasePrice(),
                    soldProductModels[0].getSoldSellingPrice(),
                    soldProductModels[0].getSoldUnit(),
                    soldProductModels[0].getStockAmount(),
                    soldProductModels[0].getSoldGroupID(),
                    soldProductModels[0].isModified()
            ));
            return null;
        }
    }

    public void update(SoldProductModel soldProductModel){
        new updateTask().execute(soldProductModel);
    }
    private class updateTask extends AsyncTask<SoldProductModel, Void, Void>{
        @Override
        protected Void doInBackground(SoldProductModel... soldProductModels) {
            appDatabase.soldProductDao().updateOne(soldProductModels[0]);
            appDatabase.productsDao().updateOne(new ProductModel(
                    soldProductModels[0].getProductID(),
                    soldProductModels[0].getSoldBarcode(),
                    soldProductModels[0].getSoldName(),
                    soldProductModels[0].getSoldDescription(),
                    soldProductModels[0].getSoldBasePrice(),
                    soldProductModels[0].getSoldSellingPrice(),
                    soldProductModels[0].getSoldUnit(),
                    soldProductModels[0].getStockAmount() - soldProductModels[0].getSoldAmount(),
                    soldProductModels[0].getSoldGroupID(),
                    soldProductModels[0].isModified()
            ));
            return null;
        }
    }


    public void deleteFactorSoldProducts(List<SoldProductModel> soldProductModels){
        new deleteFactorProductsTask().execute(soldProductModels);
    }
    private class deleteFactorProductsTask extends AsyncTask<List<SoldProductModel>, Void, Void>{
        @Override
        protected Void doInBackground(List<SoldProductModel>... lists) {
            for (SoldProductModel model : lists[0]){
                appDatabase.soldProductDao().delete(model);
                appDatabase.productsDao().updateOne(new ProductModel(
                        model.getProductID(),
                        model.getSoldBarcode(),
                        model.getSoldName(),
                        model.getSoldDescription(),
                        model.getSoldBasePrice(),
                        model.getSoldSellingPrice(),
                        model.getSoldUnit(),
                        model.getStockAmount(),
                        model.getSoldGroupID(),
                        model.isModified()
                ));
            }
            return null;
        }
    }

    //region Insert Factor
    public void insertFactor(Activity context, FactorModel factorModel, CreateFactorContent createFactorContent) {
        insertObservable(factorModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyToast.error(context.getString(R.string.store_factor_failed) + " | " + e.getMessage()).show();
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        createFactorContent.clearEntries(context);
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onComplete()");
                    }
                });
    }

    private Observable<Void> insertObservable(FactorModel factorModel) {
        return new Observable<Void>() {
            @Override
            protected void subscribeActual(Observer<? super Void> observer) {
                appDatabase.factorsDao().insertOneFactor(factorModel);
                observer.onSubscribe(new Disposable() {
                    @Override
                    public void dispose() {

                    }

                    @Override
                    public boolean isDisposed() {
                        return false;
                    }
                });
                observer.onComplete();
            }
        };
    }
    //endregion


    //region Insert Sold Product
    public void insertSoldProducts(Activity context, SoldProductModel soldProductModels, CreateFactorContent createFactorContent) {
        insertSoldProductObservable(soldProductModels)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyToast.error(context.getString(R.string.store_product_failed) + " | " + e.getMessage()).show();
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        MyLog.LogDebugging("CreateFactorActivity() ---> " + "insertFactor() ---> " + "onComplete()");
                    }
                });
    }

    private Observable<Void> insertSoldProductObservable(SoldProductModel soldProductModels) {
        return new Observable<Void>() {
            @Override
            protected void subscribeActual(Observer<? super Void> observer) {
                appDatabase.soldProductDao().insertOne(soldProductModels);
                observer.onSubscribe(new Disposable() {
                    @Override
                    public void dispose() {

                    }

                    @Override
                    public boolean isDisposed() {
                        return false;
                    }
                });
                observer.onComplete();
            }
        };
    }
    //endregion


    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null){
            disposable.dispose();
        }
    }
}
