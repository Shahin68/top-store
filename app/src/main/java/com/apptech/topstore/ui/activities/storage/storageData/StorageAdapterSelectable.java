package com.apptech.topstore.ui.activities.storage.storageData;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.ui.activities.storage.StorageViewModel;
import com.apptech.topstore.ui.activities.storage.contents.StorageContent;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StorageAdapterSelectable extends RecyclerView.Adapter<StorageAdapterSelectable.myViewHolder>{
    List<ProductModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private StorageViewModel mViewModel;
    private StorageContent mContent;
    private boolean mFiltered;

    public Context getContext() {
        return mContext;
    }
    String mSearchPhrase;

    public StorageAdapterSelectable(Context context, List<ProductModel> arrayList, String searchPhrase, StorageContent storageContent, StorageViewModel viewModel, boolean filtered){
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);
        this.mSearchPhrase = searchPhrase;
        this.mViewModel = viewModel;
        this.mContent = storageContent;
        this.mFiltered = filtered;

        Collections.sort(items, (o1, o2) -> {
            if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit() +"-"+o1.getProductBarcode()).contains(latinNumOf(searchPhrase)) && !(o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).contains(latinNumOf(searchPhrase))){
                return -1;
            } else if (!(o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit()+"-"+o1.getProductBarcode()).contains(latinNumOf(searchPhrase)) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).contains(latinNumOf(searchPhrase))){
                return 1;
            } else if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit()+"-"+o1.getProductBarcode()).contains(latinNumOf(searchPhrase)) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).contains(latinNumOf(searchPhrase))){
                if (mFiltered){
                    return Boolean.compare(o2.isModified(),o1.isModified()); // TODO Respect the last sort based on Filter - you need to find a way to keep the filtered sort after search in cleared Here
                } else {
                    return o2.getProductID().compareToIgnoreCase(o1.getProductID());
                }
            } else {
                if (mFiltered){
                    return Boolean.compare(o2.isModified(),o1.isModified()); // TODO Respect the last sort based on Filter - you need to find a way to keep the filtered sort after search in cleared Here
                } else {
                    return o2.getProductID().compareToIgnoreCase(o1.getProductID());
                }
            }
        });

    }

    private String latinNumOf(String num) {
        return ConvertNumbers.convertToLatin(num);
    }

    @Override
    public StorageAdapterSelectable.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_storage_selectable, parent, false);
        return new StorageAdapterSelectable.myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(StorageAdapterSelectable.myViewHolder holder, int position) {



        if (!mSearchPhrase.isEmpty()){
            holder.mTitle.setText(Html.fromHtml(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductName()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + String.valueOf(items.get(position).getProductSellingPrice()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>") + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
            if (items.get(position).getProductDescription().isEmpty()){
                holder.mInfo.setVisibility(View.GONE);
            } else {
                holder.mInfo.setVisibility(View.VISIBLE);
                holder.mInfo.setText(Html.fromHtml(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductDescription()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));
            }
            holder.mBarcode.setText(Html.fromHtml(ConvertNumbers.parseNumbers(mContext.getString(R.string.product_barcode) + " " +String.valueOf(items.get(position).getProductBarcode()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));
            holder.mAmount.setText(Html.fromHtml(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductAmount() + " " + items.get(position).getProductUnit()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));

            if (!(items.get(position).getProductName()+"-"+ items.get(position).getProductAmount() +"-"+ items.get(position).getProductSellingPrice() +"-"+ items.get(position).getProductUnit() +"-"+ items.get(position).getProductBarcode()).contains(latinNumOf(mSearchPhrase))){
                holder.itemView.setAlpha(.3f);
            } else {
                holder.itemView.setAlpha(1);
            }
        } else {
            holder.mTitle.setText(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductName()), false));
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
            if (items.get(position).getProductDescription().isEmpty()){
                holder.mInfo.setVisibility(View.GONE);
            } else {
                holder.mInfo.setText(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductDescription()), false));
                holder.mInfo.setVisibility(View.VISIBLE);
            }
            holder.mBarcode.setText(mContext.getString(R.string.product_barcode) + " " +ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductBarcode()), false));
            holder.mAmount.setText(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductAmount()), false)  + " " + items.get(position).getProductUnit());


            holder.itemView.setAlpha(1);
        }



        if (mContent.mSelectedProducts.contains(items.get(position))){
            holder.mCheckBox.setChecked(true);
        } else {
            holder.mCheckBox.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemStorageSelectableProduct) TextView mTitle;
        @BindView(R.id.itemStorageSelectableInfo) TextView mInfo;
        @BindView(R.id.itemStorageSelectableUnitPrice) TextView mOneUnitPrice;
        @BindView(R.id.itemStorageSelectableBarcode) TextView mBarcode;
        @BindView(R.id.itemStorageSelectableAmount) TextView mAmount;

        @BindView(R.id.itemStorageSelectableCheckbox) CheckBox mCheckBox;

        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mTitle.setTypeface(Fonts.BOLD(mContext));
            mInfo.setTypeface(Fonts.BOLD(mContext));
            mOneUnitPrice.setTypeface(Fonts.BOLD(mContext));
            mAmount.setTypeface(Fonts.BOLD(mContext));
            mBarcode.setTypeface(Fonts.NORMAL(mContext));



            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }

            mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked){
                    if (!mContent.mSelectedProducts.contains(items.get(getAdapterPosition()))){
                        mContent.mSelectedProducts.add(items.get(getAdapterPosition()));
                    }
                } else {
                    if (mContent.mSelectedProducts.contains(items.get(getAdapterPosition()))){
                        mContent.mSelectedProducts.remove(items.get(getAdapterPosition()));
                    }
                }
            });


            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (!mContent.mSelectedProducts.contains(items.get(getAdapterPosition()))){
                if (items.get(getAdapterPosition()).getProductAmount() >= 1) {
                    mCheckBox.setChecked(true);
                } else {
                    MyToast.warning(mContext.getString(R.string.non_in_stock)).show();
                }
            } else {
                mCheckBox.setChecked(false);
            }
            MyLog.LogDebugging("----------------> SELECTED ITEMS = " + mContent.mSelectedProducts);
        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.getProductFilters(position));
        mViewModel.delete(items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
    }
    private void showUndoSnackBar(int position, ProductModel productModel) {
        View view = StorageActivity.getInstance().findViewById(R.id.storage_layout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, productModel));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(productModel);
            }
        });
    }
    private void undoDelete(int position, ProductModel productModel) {
        items.add(position, productModel);
        notifyItemInserted(position);
    }
}
