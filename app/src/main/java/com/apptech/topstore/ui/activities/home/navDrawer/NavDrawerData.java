package com.apptech.topstore.ui.activities.home.navDrawer;


import android.util.Pair;

import com.apptech.topstore.R;

public class NavDrawerData {
    public static Pair<Integer, Integer>[] data(){
        Pair[] pairs = new Pair[]{
                new Pair(R.string.profile, R.drawable.ic_profile),
                new Pair(R.string.users, R.drawable.ic_users),
                new Pair(R.string.customers, R.drawable.ic_customers),
                new Pair(R.string.invite_friends, R.drawable.ic_share),
                new Pair(R.string.rate_review, R.drawable.ic_star),
                new Pair(R.string.settings, R.drawable.ic_cog),
                new Pair(R.string.support, R.drawable.ic_headphones),
                new Pair(R.string.about_us, R.drawable.ic_topstore_logo)
        };
        return pairs;
    }
}
