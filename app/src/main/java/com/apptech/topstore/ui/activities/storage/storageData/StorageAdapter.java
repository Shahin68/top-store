package com.apptech.topstore.ui.activities.storage.storageData;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.ui.activities.storage.StorageViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StorageAdapter extends RecyclerView.Adapter<StorageAdapter.myViewHolder>{
    List<ProductModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private StorageViewModel mViewModel;
    private boolean mFiltered;

    public Context getContext() {
        return mContext;
    }
    String mSearchPhrase;

    public StorageAdapter(Context context, List<ProductModel> arrayList, String searchPhrase, StorageViewModel viewModel, boolean filtered){
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);
        this.mSearchPhrase = searchPhrase;
        this.mViewModel = viewModel;
        this.mFiltered = filtered;

        Collections.sort(items, (o1, o2) -> {
            if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit() +"-"+o1.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase())) && !(o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase()))){
                return -1;
            } else if (!(o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit()+"-"+o1.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase())) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase()))){
                return 1;
            } else if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+o1.getProductUnit()+"-"+o1.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase())) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+o2.getProductUnit()+"-"+o2.getProductBarcode()).toLowerCase().contains(latinNumOf(searchPhrase.toLowerCase()))){
                if (mFiltered){
                    return 0; // TODO Respect the last sort based on Filter - you need to find a way to keep the filtered sort after search in cleared Here
                } else {
                    return o2.getProductID().compareToIgnoreCase(o1.getProductID());
                }
            } else {
                if (mFiltered){
                    return 0; // TODO Respect the last sort based on Filter - you need to find a way to keep the filtered sort after search in cleared Here
                } else {
                    return o2.getProductID().compareToIgnoreCase(o1.getProductID());
                }
            }
        });

    }

    private String latinNumOf(String num) {
        return ConvertNumbers.convertToLatin(num);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_storage, parent, false);
        return new myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {



        if (!mSearchPhrase.isEmpty()){
            holder.mTitle.setText(Html.fromHtml(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductName()).toLowerCase().replaceAll(latinNumOf(mSearchPhrase.toLowerCase()), "<font color='red'>" + latinNumOf(mSearchPhrase.toLowerCase()) + "</font>"), false)));
            holder.mAmount.setText(Html.fromHtml(ConvertNumbers.parseNumbers((items.get(position).getProductAmount() + " " + items.get(position).getProductUnit()).toLowerCase().replaceAll(latinNumOf(mSearchPhrase.toLowerCase()), "<font color='red'>"+latinNumOf(mSearchPhrase.toLowerCase())+"</font>"), false)));
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + String.valueOf(items.get(position).getProductSellingPrice()).toLowerCase().replaceAll(latinNumOf(mSearchPhrase.toLowerCase()), "<font color='red'>"+latinNumOf(mSearchPhrase.toLowerCase())+"</font>") + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
            holder.mBarcode.setText(Html.fromHtml(mContext.getString(R.string.product_barcode) + " " +ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductBarcode()).toLowerCase().replaceAll(latinNumOf(mSearchPhrase.toLowerCase()), "<font color='red'>" + latinNumOf(mSearchPhrase.toLowerCase()) + "</font>"), false)));


            if (!(items.get(position).getProductName()+"-"+ items.get(position).getProductAmount() +"-"+ items.get(position).getProductSellingPrice() +"-"+items.get(position).getProductUnit()+"-"+items.get(position).getProductBarcode()).toLowerCase().contains(latinNumOf(mSearchPhrase.toLowerCase()))){
                holder.itemView.setAlpha(.3f);
            } else {
                holder.itemView.setAlpha(1);
            }
        } else {
            holder.mTitle.setText(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductName()), false));
            holder.mAmount.setText(ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductAmount()), false) + " " + items.get(position).getProductUnit());
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
            holder.mBarcode.setText(mContext.getString(R.string.product_barcode) + " " + ConvertNumbers.parseNumbers(String.valueOf(items.get(position).getProductBarcode()), false));

            holder.itemView.setAlpha(1);
        }




    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemStorageProduct) TextView mTitle;
        @BindView(R.id.itemStorageAmount) TextView mAmount;
        @BindView(R.id.itemStorageUnitPrice) TextView mOneUnitPrice;
        @BindView(R.id.itemStorageBarcode) TextView mBarcode;


        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mTitle.setTypeface(Fonts.BOLD(mContext));
            mAmount.setTypeface(Fonts.BOLD(mContext));
            mOneUnitPrice.setTypeface(Fonts.BOLD(mContext));
            mBarcode.setTypeface(Fonts.NORMAL(mContext));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }




            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.getProductFilters(position));
        mViewModel.delete(items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
    }
    private void showUndoSnackBar(int position, ProductModel productModel) {
        View view = StorageActivity.getInstance().findViewById(R.id.storage_layout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, productModel));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(productModel);
            }
        });
    }
    private void undoDelete(int position, ProductModel productModel) {
        items.add(position, productModel);
        notifyItemInserted(position);
    }
}
