package com.apptech.topstore.ui.activities.createFactor.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.GenerateFactorID;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.soldProducts.SoldProductModel;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorActivity;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorViewModel;
import com.apptech.topstore.ui.activities.createFactor.SoldProducts;
import com.apptech.topstore.ui.activities.createFactor.productsData.FactorsSoldProductsAdapter;
import com.apptech.topstore.ui.activities.createFactor.productsData.FactorsSoldProductsAdapterSwipeToDeleteCallback;
import com.apptech.topstore.ui.activities.customerName.CustomerName;
import com.apptech.topstore.ui.activities.newProduct.NewProductActivity;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.bumptech.glide.Glide;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.PowerMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateFactorContent {
    CreateFactorViewModel mViewModel;
    List<SoldProductModel> mSoldProductsModel;
    // --- toolbar views
    @BindView(R.id.createFactorTitle) TextView mTitle;
    @BindView(R.id.createFactorBack) LinearLayout mBack;
    @BindView(R.id.createFactorTopDecor) AppCompatImageView mTopDecor;
    @BindView(R.id.createFactorBtn) Button mStoreFactor;

    // ---- contents
    @BindView(R.id.factorNewProduct) CardView mNewProduct;
    @BindView(R.id.factorAddProduct) CardView mAddProduct;
    @BindView(R.id.factorBarcodeReader) CardView mBarcodeReader;
    @BindView(R.id.factorProductLlist) RecyclerView mProductList;

    @BindView(R.id.createFactorCustomer) LinearLayout mAddCustomer; // Clickable
    @BindView(R.id.createFactorCustomerTxt) TextView mCustomerTxt;
    @BindView(R.id.createFactorCost) TextView mFactorPrice;



    int dim8;

    public int NEW_FACTOR_ID = 0;
    private String CUSTOMER_NAME = "";
    private String CUSTOMER_PHONE = "";
    private int FACTOR_PRODUCT_COUNT = 0;
    private int FACTOR_PRICE = 0;

    public CreateFactorContent(Activity activity, int type, CreateFactorViewModel viewModel){
        this.mViewModel = viewModel;
        ButterKnife.bind(this, activity);
        dim8 = activity.getResources().getDimensionPixelSize(R.dimen.dimen8);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.BOLD(activity));
        mCustomerTxt.setTypeface(Fonts.BOLD(activity));
        mFactorPrice.setTypeface(Fonts.BOLD(activity));
        mStoreFactor.setTypeface(Fonts.BOLD(activity));
        mFactorPrice.setText("0");
        SoldProducts.clear();
        SoldProducts.setList(new ArrayList<>());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mNewProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mBarcodeReader.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddCustomer.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mStoreFactor.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        {
            switch (type){
                case 0:
                    mTitle.setText(R.string.rejected_factor);
                    break;
                case 1:
                    mTitle.setText(R.string.purchase_factor);
                    break;
                case 2:
                    mTitle.setText(R.string.sale_factor);
                    break;
            }
        }


        viewModel.getFactors().observe(CreateFactorActivity.getInstance(), factorModels -> {
            if (factorModels.isEmpty()){
                NEW_FACTOR_ID = 0;
            } else {
                NEW_FACTOR_ID = GenerateFactorID.newID_Descending(factorModels);
            }
        });






        // create new product
        mNewProduct.setOnClickListener(v -> {
            Intent intent = new Intent(activity, NewProductActivity.class);
            intent.putExtra("new_product_type", 2);
            activity.startActivityForResult(intent, 1);
        });
        // find & getProductFilters an existing product
        mAddProduct.setOnClickListener(v -> {
            Intent intent = new Intent(activity, StorageActivity.class);
            intent.putExtra("storage_type", 2);
            activity.startActivityForResult(intent, 1);
        });
        // add a product with barcode reader
        mBarcodeReader.setOnClickListener(v -> {
            MyToast.error(activity.getString(R.string.in_development)).show();
        });
        mAddCustomer.setOnClickListener(v -> {
            Intent intent = new Intent(activity, CustomerName.class);
            activity.startActivityForResult(intent, 2);
        });
        // --- create new product
        mStoreFactor.setOnClickListener(v -> {
            if (FACTOR_PRICE == 0){
                MyToast.warning(activity.getString(R.string.factors_is_empty)).show();
            } else {
                createFactor(
                        activity,
                        String.valueOf(NEW_FACTOR_ID),
                        String.valueOf(NEW_FACTOR_ID),
                        System.currentTimeMillis(),
                        CUSTOMER_NAME,
                        CUSTOMER_PHONE,
                        type,
                        FACTOR_PRODUCT_COUNT,
                        FACTOR_PRICE
                );
            }
        });
    }
    public void setCustomerName(String name, String phone){
        mCustomerTxt.setText(name);
        CUSTOMER_NAME = name;
        CUSTOMER_PHONE = phone;
    }
    public void setFactorPrice(int count, int cost){
        mFactorPrice.setText(ConvertNumbers.parseNumbers(String.valueOf(cost), true));
        FACTOR_PRODUCT_COUNT = count;
        FACTOR_PRICE = cost;
    }
    public void setProductList(Activity context, List<SoldProductModel> soldProductModel){
        mSoldProductsModel = soldProductModel;

        FactorsSoldProductsAdapter adapter = new FactorsSoldProductsAdapter(context, soldProductModel, this, mViewModel);
        mProductList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsSoldProductsAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mProductList);
        adapter.notifyDataSetChanged();
        mProductList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));


        int cost = 0;
        for (int i = 0; i < soldProductModel.size(); i++){
            cost = cost + (soldProductModel.get(i).getSoldAmount()*soldProductModel.get(i).getSoldSellingPrice());
        }

        setFactorPrice(soldProductModel.size(), cost);
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "setProductList() ---> " + "| DONE");
        context.setResult(0);
    }
    public void updateFactorPrice(List<SoldProductModel> soldProductModel){
        int cost = 0;
        for (int i = 0; i < soldProductModel.size(); i++){
            cost = cost + (soldProductModel.get(i).getSoldAmount()*soldProductModel.get(i).getSoldSellingPrice());
        }
        setFactorPrice(soldProductModel.size(), cost);
    }
    private void createFactor(Activity activity, String factorID, String factorReferenceCode, long date, String customerName, String customerPhone, int type, int productCount, int cost){
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "CreateFactorContent() ---> " + "createFactor()" + "\n" +
                "FACTOR_ID = " + factorID + "\n" +
                "REFERENCE_ID = " + factorReferenceCode + "\n" +
                "DATE = " + date + "\n" +
                "CUSTOMER_NAME = " + customerName + "\n" +
                "CUSTOMER_PHONE = " + customerPhone + "\n" +
                "FACTOR_STATUS = " + 1 + "\n" +
                "USER_ID = " + MySharedPreferences.getInteger(MyPreferences.USER_ID, 0) + "\n" +
                "FACTOR_TYPE = " + type + "\n" +
                "PRODUCT_COUNT = " + productCount + "\n" +
                "FACTOR_COST = " + cost);
        mViewModel.insertFactor(activity, new FactorModel(
                factorID,
                factorReferenceCode,
                date,
                customerName,
                customerPhone,
                1,
                MySharedPreferences.getInteger(MyPreferences.USER_ID, 0),
                type,
                productCount,
                cost
        ), this);
    }

    public void clearEntries(Activity context) {
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "CreateFactorContent() ---> " + "clearEntries()");
        // TODO Clear Everything When Factor is Created
        context.finish();
    }

    public void backPresses(Activity activity) {
        if (FACTOR_PRICE == 0){
            activity.finish();
        } else {
            mViewModel.deleteFactorSoldProducts(mSoldProductsModel);
            showFactorRemoveWarningDialog(activity, mTitle);
        }
    }
    private void showFactorRemoveWarningDialog(Activity activity, View view){
        PowerMenu powerMenu = new PowerMenu.Builder(activity)
                .setHeaderView(R.layout.factor_remove_header)
                .setFooterView(R.layout.factor_remove_footer)
                .setAnimation(MenuAnimation.FADE)
                .setMenuRadius(26f)
                .setMenuShadow(10f)
                .setWidth(activity.getResources().getDisplayMetrics().widthPixels - activity.getResources().getDimensionPixelSize(R.dimen.dimen24))
                .setTextSize(15)
                .setTextColor(ContextCompat.getColor(activity, R.color.textBlack))
                .setTextTypeface(Fonts.BOLD(activity))
                .setTextGravity(Gravity.CENTER)
                .setSelectedEffect(false)
                .setShowBackground(true)
                .setBackgroundAlpha(.2f)
                .build();
        powerMenu.showAtCenter(view);


        // ---- Footer Content
        TextView mInfo = powerMenu.getHeaderView().findViewById(R.id.header_title);
        mInfo.setTypeface(Fonts.BOLD(activity));

        // ---- Footer Content
        Button mNo = powerMenu.getFooterView().findViewById(R.id.footer_negative);
        Button mYes = powerMenu.getFooterView().findViewById(R.id.footer_positive);
        mNo.setBackgroundColor(ContextCompat.getColor(activity,R.color.white));
        mYes.setBackgroundColor(ContextCompat.getColor(activity,R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mNo.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mYes.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
        }

        mNo.setOnClickListener(v -> powerMenu.dismiss());
        mYes.setOnClickListener(v -> {
            powerMenu.dismiss();
            activity.finish();
        });
    }

}
