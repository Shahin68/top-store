package com.apptech.topstore.ui.activities.calculator.contents;

import android.app.Activity;
import android.os.Build;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.utils.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalculatorContent {

    // --- Calculator Result Text
    @BindView(R.id.calculatorResultTxt) TextView mResultTxt;

    // --- Calculator Main Buttons
    @BindView(R.id.calculatorBackBtn) Button mBackBtn;
    @BindView(R.id.calculatorConfirmBtn) Button mConfirmBtn;

    // --- Calculator Function Buttons
    @BindView(R.id.calculatorFunNum0) Button mNum0;
    @BindView(R.id.calculatorFunNum1) Button mNum1;
    @BindView(R.id.calculatorFunNum2) Button mNum2;
    @BindView(R.id.calculatorFunNum3) Button mNum3;
    @BindView(R.id.calculatorFunNum4) Button mNum4;
    @BindView(R.id.calculatorFunNum5) Button mNum5;
    @BindView(R.id.calculatorFunNum6) Button mNum6;
    @BindView(R.id.calculatorFunNum7) Button mNum7;
    @BindView(R.id.calculatorFunNum8) Button mNum8;
    @BindView(R.id.calculatorFunNum9) Button mNum9;
    @BindView(R.id.calculatorFunZeroTriple) Button mTripleZero;
    @BindView(R.id.calculatorFunDot) Button mDot;
    @BindView(R.id.calculatorFunBackscape) AppCompatImageButton mBackScape;

    @BindView(R.id.calculatorFunEqual) Button mEqual;
    @BindView(R.id.calculatorFunSubtract) Button mSubtract;
    @BindView(R.id.calculatorFunSum) Button mSum;
    @BindView(R.id.calculatorFunMultiply) Button mMultiply;
    @BindView(R.id.calculatorFunDivision) Button mDivision;
    @BindView(R.id.calculatorFunPercentage) Button mPercentage;
    @BindView(R.id.calculatorFunClear) Button mClear;



    public CalculatorContent(Activity activity){
        ButterKnife.bind(this, activity);
        mResultTxt.setTypeface(Fonts.BOLD(activity));
        mBackBtn.setTypeface(Fonts.BOLD(activity));
        mConfirmBtn.setTypeface(Fonts.BOLD(activity));
        mNum0.setTypeface(Fonts.BOLD(activity));
        mNum1.setTypeface(Fonts.BOLD(activity));
        mNum2.setTypeface(Fonts.BOLD(activity));
        mNum3.setTypeface(Fonts.BOLD(activity));
        mNum4.setTypeface(Fonts.BOLD(activity));
        mNum5.setTypeface(Fonts.BOLD(activity));
        mNum6.setTypeface(Fonts.BOLD(activity));
        mNum7.setTypeface(Fonts.BOLD(activity));
        mNum8.setTypeface(Fonts.BOLD(activity));
        mNum9.setTypeface(Fonts.BOLD(activity));
        mTripleZero.setTypeface(Fonts.BOLD(activity));
        mDot.setTypeface(Fonts.BOLD(activity));
        mEqual.setTypeface(Fonts.BOLD(activity));
        mSubtract.setTypeface(Fonts.BOLD(activity));
        mSum.setTypeface(Fonts.BOLD(activity));
        mMultiply.setTypeface(Fonts.BOLD(activity));
        mDivision.setTypeface(Fonts.BOLD(activity));
        mPercentage.setTypeface(Fonts.BOLD(activity));
        mClear.setTypeface(Fonts.BOLD(activity));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBackBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mConfirmBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));

            mNum0.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum1.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum2.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum3.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum4.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum5.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum6.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum7.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum8.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mNum9.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));

            mTripleZero.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mDot.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mBackScape.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));

            mEqual.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mSubtract.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mSum.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mMultiply.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mDivision.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mPercentage.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mClear.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
        }




        mBackBtn.setOnClickListener(v -> activity.finish());
        mConfirmBtn.setOnClickListener(v -> {
            finishActivity(activity);
        });
    }
    // TODO return new value calculated to be applied on the price of selling item in newFactors
    public void finishActivity(Activity context) {
        /*MyLog.LogDebugging("CustomerName() ---> " + "NewProductContent() ---> " + "finishActivity()");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("customer_name", customerName);
        returnIntent.putExtra("customer_phone", phone);
        context.setResult(1, returnIntent);*/
        context.finish();
    }

}
