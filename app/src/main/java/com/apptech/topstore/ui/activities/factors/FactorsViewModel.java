package com.apptech.topstore.ui.activities.factors;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.soldProducts.SoldProductModel;

import java.util.List;

import javax.inject.Inject;

public class FactorsViewModel extends ViewModel {
    @Inject
    AppDatabase appDatabase;

    public LiveData<List<SoldProductModel>> getFactorProducts(String id) {
        return appDatabase.soldProductDao().findSoldProductsByFactorID(id);
    }

    public LiveData<List<FactorModel>> getFactors() {
        return appDatabase.factorsDao().getAllFactorsLive();
    }

    public void delete(FactorModel factorModel){
        new deleteTask().execute(factorModel);
    }
    private class deleteTask extends AsyncTask<FactorModel, Void, Void>{
        @Override
        protected Void doInBackground(FactorModel... factorModels) {
            appDatabase.factorsDao().deleteFactor(factorModels[0]);
            return null;
        }
    }

    public void deleteFactorSoldProducts(FactorsActivity factorsActivity, FactorModel factorModel){
        getFactorProducts(factorModel.getFactorID()).observe(factorsActivity, new Observer<List<SoldProductModel>>() {
            @Override
            public void onChanged(List<SoldProductModel> soldProductModels) {
                new deleteFactorProductsTask().execute(soldProductModels);
            }
        });
    }
    private class deleteFactorProductsTask extends AsyncTask<List<SoldProductModel>, Void, Void>{
        @Override
        protected Void doInBackground(List<SoldProductModel>... lists) {
            for (SoldProductModel model : lists[0]){
                appDatabase.soldProductDao().delete(model);
            }
            return null;
        }
    }

    public void insert(FactorModel factorModel) { appDatabase.factorsDao().insertOneFactor(factorModel); }

    public LiveData<List<FactorModel>> getAllRegardless(int sortingTypes) {
        if (sortingTypes == 0){
            return appDatabase.factorsDao().getFactorsNewest();
        } else if (sortingTypes == 1) {
            return appDatabase.factorsDao().getFactorsOldest();
        } else if (sortingTypes == 2) {
            return appDatabase.factorsDao().getFactorsMostExpensive();
        } else if (sortingTypes == 3) {
            return appDatabase.factorsDao().getFactorsLeastExpensive();
        } else if (sortingTypes == 4) {
            return appDatabase.factorsDao().getAllFactorsLive();
        } else {
            return appDatabase.factorsDao().getAllFactorsLive();
        }
    }
}
